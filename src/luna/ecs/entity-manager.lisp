;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:luna.ecs)

(defdyn *entity-manager*
  :documentation "The 'active' entity manager to use for `add-entity', and `create-entity'.")

(defclass entity-manager ()
  ((global-events
    :type event-manager
    :initarg :event-manager
    :initform (error "entity-manager: must supply event-manager")
    :reader global-events)
   (entities
    :type (vector t *)
    :initform (make-array 10 :adjustable t :fill-pointer 0)
    :accessor entities)
   (removal-queue
    :type list
    :initform nil
    :accessor entity-removal-queue)))

(defmethod initialize-instance :after ((manager entity-manager) &key &allow-other-keys)
  (subscribe-event
   (global-events manager)
   :time-tick
   manager
   '%entity-manager-on-time-tick
   :priority 0))

(defun create-entity (&key (entity-manager *entity-manager*) (name "Entity") attributes)
  (let ((entity (make-instance 'entity :name name :attributes attributes)))
    (setf (slot-value entity 'entity-manager) entity-manager)
    (vector-push-extend entity (entities entity-manager))
    entity))

(defun add-entity (entity &key (entity-manager *entity-manager*))
  (when (slot-boundp entity 'entity-manager)
    (error "entity-manager: entity already belongs to other entity-manager ~A" entity))
  (setf (slot-value entity 'entity-manager) entity-manager)
  (vector-push-extend entity (entities entity-manager) 10)
  (values))

(defmacro with-components (entity (&rest bindings) &body body)
  (once-only ((entity-sym entity))
    (let ((name-type-opt
           (mapcar (lambda (b)
                     (list
                      (car b)
                      (cadr b)
                      (eq (caddr b) '&optional)))
                   bindings)))
      ;;Bind the required vars first
      `(when-let ,(mapcan
                   (lambda (n-t-o)
                     (let ((name (car n-t-o))
                           (type (cadr n-t-o))
                           (optional (caddr n-t-o)))
                       (if (not optional)
                           (list
                            `(,name (get-component ,entity-sym ',type)))
                           nil)))
                   name-type-opt)
         ;;Then the optional ones
         (let ,(mapcan
                (lambda (n-t-o)
                  (let ((name (car n-t-o))
                        (type (cadr n-t-o))
                        (optional (caddr n-t-o)))
                    (if optional
                        (list
                         `(,name (get-component ,entity-sym ',type)))
                        nil)))
                name-type-opt)
           ,@body)))))

(defmacro do-components ((entity &rest bindings) (&key (entity-manager '*entity-manager*)) &body body)
  (once-only ((em-sym entity-manager))
    ;;Look at each entity
    `(loop
        :for ,entity :across (entities ,em-sym)
        :do
        ;;Bind the required vars first
        (with-components ,entity ,bindings
          ,@body))))

(defun %entity-manager-on-time-tick (manager dt)
  (declare (ignore dt))

  (dolist (entity (nreverse (entity-removal-queue manager)))
    (setf (entities manager) (delete entity (entities manager)))
    (dolist (c (list-components entity))
      (remove-component entity (type-of c)))

    (slot-makunbound entity 'entity-manager))

  (setf (entity-removal-queue manager) nil))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:luna.ecs)

(defclass entity ()
  ((name
    :type string
    :initarg :name
    :initform "Entity"
    :accessor name)
   (entity-manager
    :type entity-manager
    :reader entity-manager)
   (entity-events
    :type event-manager
    :initform (make-instance 'event-manager)
    :reader entity-events)
   (components
    :type hash-table
    :initform (make-hash-table :test 'eq)
    :reader entity-components)
   (attributes
    :type list
    :initarg :attributes
    :initform '()
    :accessor attributes)))

(defmethod print-object ((entity entity) stream)
  (print-unreadable-object (entity stream :type t)
    (format stream "~A" (name entity))))

(defmethod serial:serialized-slots ((entity entity))
  '(name))

(defmethod global-events ((entity entity))
  (global-events (entity-manager entity)))

(defun list-components (entity)
  (hash-table-values (entity-components entity)))

(defun add-component (entity component
                      &aux
                        (type (type-of component)))
  (prog1
      (setf (gethash type (entity-components entity)) component)
    (component-init component entity)
    (send-event
     (global-events entity)
     :components-changed
     entity)))

(defun get-component (entity type)
  (values (gethash type (entity-components entity))))

(defun ensure-component (entity type
                         &optional (default nil default-sup-p))
  (let ((component (get-component entity type)))
    (unless component
      (if default-sup-p
          (setf component default)
          (setf component (make-instance type)))
      (add-component entity component))
    component))

(defun remove-component (entity type)
  (when-let ((component (gethash type (entity-components entity))))
    (remhash type (entity-components entity))
    (component-uninit component entity)
    (send-event
     (global-events entity)
     :components-changed
     entity))
  (values))

(defun destroy-entity (entity &aux (entity-manager (entity-manager entity)))
  (pushnew entity (entity-removal-queue entity-manager))
  (values))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:luna.ecs)

(defclass component-metaclass (standard-class)
  ())

(defmethod validate-superclass ((sub component-metaclass) (super standard-class))
  t)

(defclass component-slot-definition (standard-slot-definition)
  ((component-slot-property-p
    :initform nil
    :initarg :property-p
    :accessor component-slot-property-p)))

(defclass component-d-slot-definition (standard-direct-slot-definition
                                       component-slot-definition)
  ())

(defmethod direct-slot-definition-class ((class component-metaclass) &rest initargs)
  (declare (ignore initargs))
  'component-d-slot-definition)

(defclass component-e-slot-definition (standard-effective-slot-definition
                                       component-slot-definition)
  ())

(defmethod effective-slot-definition-class ((class component-metaclass) &rest initargs)
  (declare (ignore initargs))
  'component-e-slot-definition)

(defmethod compute-effective-slot-definition :around ((class component-metaclass) name direct-slots)
  (declare (ignore name))
  (let ((eslot (call-next-method))
        (dslot (first direct-slots)))
    (setf (component-slot-property-p eslot) (component-slot-property-p dslot))
    eslot))

(in-package #:luna.ecs)

(defmacro defdyn (name &key (value nil value-p) (documentation nil documentation-p))
  "As `defvar', but allows specifying documentation without binding to a value."
  (if value-p
      `(defvar ,name ,value ,documentation)
      `(progn
         (defvar ,name)
         ,(when documentation-p
            `(setf (documentation ',name 'variable) ',documentation))
         ',name)))

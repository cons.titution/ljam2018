;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:defpackage+-user-1)

(defpackage+ #:luna.ecs
  (:use
   #:alexandria
   #:closer-common-lisp)
  (:local-nicknames
   (#:serial #:luna.serial))
  (:export
   #:entity-manager
   #:global-events
   #:entity-events

   #:event-manager
   #:forward-event
   #:unforward-event
   #:subscribe-event
   #:unsubscribe-event
   #:send-event

   #:component
   #:component-entity
   #:entity-manager
   #:global-events
   #:entity-events
   #:component-init
   #:component-uninit

   #:define-component

   #:entity
   #:name
   #:entity-manager
   #:entity-events
   #:global-events
   #:attributes

   #:*entity-manager*
   #:entity-manager
   #:entities
   #:global-events

   #:create-entity
   #:add-entity
   #:destroy-entity

   #:with-components
   #:do-components

   #:add-component
   #:list-components
   #:get-component
   #:ensure-component
   #:remove-component

   #:system
   #:entity-manager
   #:event-manager
   #:system-configure
   #:system-unconfigure

   #:system-manager
   #:add-system
   #:configure-systems
   #:unconfigure-systems))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defsystem #:luna
  :name "luna"
  :version "0.0.0.0"
  :description "Game System"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :serial t
  :components
  ((:file "package")
   (:module "serial"
    :serial t
    :components
    ((:file "package")
     (:file "serial")))
   (:module "math"
    :serial t
    :components
    ((:file "package")
     (:file "vector")
     (:file "quaternion")))
   (:module "shapes"
    :serial t
    :components
    ((:file "package")
     (:file "poly")))
   (:module "control"
    :serial t
    :components
    ((:file "package")
     (:file "player-handle")))
   (:module "physics"
    :serial t
    :components
    ((:file "package")
     (:file "collision")))
   (:module "ecs"
    :serial t
    :components
    ((:file "package")
     (:file "util")
     (:file "generic")
     (:file "event-manager")
     (:file "component-metaclass")
     (:file "entity")
     (:file "component")
     (:file "entity-manager")
     (:file "system")
     (:file "system-manager"))))
  :depends-on
  (#:alexandria
   #:defpackage-plus
   #:closer-mop
   #:marshal
   #:sol))

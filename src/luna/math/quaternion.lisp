;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package :luna.math)

(defclass quaternion (serial:serializable)
  ((quat-w
    :type real :initform 1.0
    :initarg :w :accessor quat-w)
   (%v
    :type vec3
    :reader %v)))

(defmethod print-object ((q quaternion) stream)
  (print-unreadable-object (q stream :type t)
    (format stream "(~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F)"
            (quat-w q)
            (quat-x q)
            (quat-y q)
            (quat-z q))))

(defmethod serial:serialized-slots ((obj quaternion))
  '(quat-w %v))

(defun quat (&key (w 0) (x 0) (y 0) (z 0))
  (declare (type real w x y z))
  (make-instance 'quaternion :w w :x x :y y :z z))

(defun quat-x (q)
  (vec-x (%v q)))

(defun (setf quat-x) (v q)
  (setf (vec-x (%v q)) v))

(defun quat-y (q)
  (vec-y (%v q)))

(defun (setf quat-y) (v q)
  (setf (vec-y (%v q)) v))

(defun quat-z (q)
  (vec-z (%v q)))

(defun (setf quat-z) (v q)
  (setf (vec-z (%v q)) v))

(defmethod initialize-instance :after ((q quaternion) &key (x 0) (y 0) (z 0))
  (setf (slot-value q '%v) (vec3 :x x :y y :z z)))

(defun quat-identity ()
  (make-instance 'quaternion))

(defvar +quat-identity+ (quat-identity))

(defun quat+ (q &rest quantities)
  (declare (type quaternion q))
  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (declare (type real res-w)
             (type vec3 res-v))

    (dolist (q quantities)
      (etypecase q
        (real
         (incf res-w q)
         (setf res-v (vec+ res-v q)))
        (vec3
         (setf res-v (vec+ res-v q)))
        (quaternion
         (incf res-w (quat-w q))
         (setf res-v (vec+ res-v (%v q))))))

    (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat- (q &rest quantities)
  (declare (type quaternion q))
  ;;Special case if no quantities to subtract
  ;;Negate quaternion (negate both angle and axis... same dir)
  (if (null quantities)
      (make-instance
       'quaternion
       :w (- (quat-w q))
       :x (- (quat-x q))
       :y (- (quat-y q))
       :z (- (quat-z q)))
      (let ((res-w (quat-w q))
            (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
        (declare (type real res-w)
                 (type vec3 res-v))

        (dolist (q quantities)
          (etypecase q
            (real
             (incf res-w q)
             (setf res-v (vec+ res-v q)))
            (vec3
             (setf res-v (vec+ res-v q)))
            (quaternion
             (incf res-w (quat-w q))
             (setf res-v (vec+ res-v (%v q))))))

        (make-instance 'quaternion :w res-w :x (quat-x res-v) :y (vec-y res-v) :z (vec-z res-v)))))

(defun quat* (q &rest quantities)
  (declare (type quaternion q))
  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (dolist (q quantities)
      (etypecase q
        (real
         (setf res-w (* res-w q))
         (setf res-v (vec* res-v q)))
        (quaternion
         (let ((sa res-w)
               (sb (quat-w q))
               (a res-v)
               (b (%v q)))
           (declare (type real sa sb)
                    (type vec3 a b))
           (setf res-w (- (* sa sb)
                          (vec-dot a b)))
           (setf res-v (vec+ (vec* b sa)
                             (vec* a sb)
                             (vec-cross a b)))))))

    (make-instance 'quaternion :w res-w :x (quat-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat/ (q num-or-quat &rest quantities)
  (declare (type quaternion q)
           (type (or real quaternion) num-or-quat))

  (let ((res-w (quat-w q))
        (res-v (vec3 :x (quat-x q) :y (quat-y q) :z (quat-z q))))
    (declare (type real res-w)
             (type vec3 res-v))

    (dolist (q (cons num-or-quat quantities))
      (etypecase q
        (real
         (setf res-w (/ res-w q))
         (setf res-v (vec/ res-v q)))
        (quaternion
         ;;Division of quaternion A by quaternion B is multiplying A by the multiplicative inverse of B
         (let ((res-quat
                (quat*
                 (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))
                 (quat-inverse q))))
           (setf res-w (quat-w res-quat))
           (setf res-v (%v res-quat))))))

    (make-instance 'quaternion :w res-w :x (vec-x res-v) :y (vec-y res-v) :z (vec-z res-v))))

(defun quat-conj (q)
  (declare (type quaternion q))
  (make-instance
   'quaternion
   :w (quat-w q)
   :x (- (quat-x q))
   :y (- (quat-y q))
   :z (- (quat-z q))))

(defun quat-scale (q)
  (declare (type quaternion q))
  (sqrt
   (+ (expt (quat-w q) 2)
      (expt (quat-x q) 2)
      (expt (quat-y q) 2)
      (expt (quat-z q) 2))))

(defun quat-norm (q)
  (declare (type quaternion q))
  (let ((length
         (quat-scale q)))
    (if (zerop length)
        (make-instance 'quaternion)
        (quat/ q length))))

(defun quat-inverse (q)
  (declare (type quaternion q))
  (quat/
   (quat-conj q)
   (+ (expt (quat-w q) 2)
      (expt (quat-x q) 2)
      (expt (quat-y q) 2)
      (expt (quat-z q) 2))))

(defun quat-dot (q1 q2)
  (declare (type quaternion q1 q2))
  (+ (* (quat-w q1) (quat-w q1))
     (vec-dot (%v q1) (%v q2))))

(defun quat-lerp (q1 q2 d)
  (declare (type quaternion q1 q2)
           (type real d))
   ;;Clamp it between 0 and 1
  (setf d (min (max d 0.0) 1.0))

  (quat-norm
   (quat+ (quat* q1 (- 1 d))
          (quat* q2 d))))

(defun quat-slerp (q1 q2 d)
  (declare (type quaternion q1 q2)
           (type real d))
 ;;Clamp it between 0 and 1
  (setf d (min (max d 0.0) 1.0))

  (let* ((dot (quat-dot q1 q2))
         (q3 q2))
    (when (< dot 0)
      (setf dot (- dot))
      (setf q3 (quat- q2)))

    (if (< dot 0.95)
        (let ((angle (acos dot)))
          (quat+ (quat* q1 (sin (* angle (- 1 d))))
                 (quat* q3 (/ (sin (* angle d))
                              (sin angle)))))
        ;;Small angles: Use lerp
        (quat-lerp q1 q3 d))))

(defun quat-from-rot (rot axis)
  (declare (type real rot)
           (type vec3 axis))
  (let ((rot-v (vec* axis (sin (/ rot 2)))))
    (make-instance
     'quaternion
     :w (cos (/ rot 2))
     :x (vec-x rot-v)
     :y (vec-y rot-v)
     :z (vec-z rot-v))))

(defun quat-look-rot (forward &optional (up +vec-up+))
  (declare (type vec3 forward up))
  (let* ((v1 (vec-norm forward))
         (v2 (vec-norm (vec-cross (vec-norm up) v1)))
         (v3 (vec-norm (vec-cross v1 v2)))
         (m00 (vec-x v2))
         (m01 (vec-y v2))
         (m02 (vec-z v2))
         (m10 (vec-x v3))
         (m11 (vec-y v3))
         (m12 (vec-z v3))
         (m20 (vec-x v1))
         (m21 (vec-y v1))
         (m22 (vec-z v2))
         (num8 (+ m00 m11 m22))
         (ret-quat (make-instance 'quaternion)))
    (cond
      ((> num8 0)
       (let ((num (sqrt (1+ num8))))
         (setf (quat-w ret-quat) (* num 0.5))
         (setf num (/ 0.5 num))
         (setf (quat-x ret-quat) (* (- m12 m21) num))
         (setf (quat-y ret-quat) (* (- m20 m02) num))
         (setf (quat-z ret-quat) (* (- m01 m10) num))))
      ((and (>= m00 m11) (>= m00 m22))
       (let* ((num7 (sqrt (- (1+ m00) m11 m22)))
              (num4 (/ 0.5 num7)))
         (setf (quat-w ret-quat) (* (- m12 m21) num4))
         (setf (quat-x ret-quat) (* 0.5 num7))
         (setf (quat-y ret-quat) (* (+ m01 m10) num4))
         (setf (quat-z ret-quat) (* (+ m02 m20) num4))))
      ((> m11 m22)
       (let* ((num6 (sqrt (- (1+ m11) m00 m22)))
              (num3 (/ 0.5 num6)))
         (setf (quat-w ret-quat) (* (- m20 m02) num3))
         (setf (quat-x ret-quat) (* (+ m10 m01) num3))
         (setf (quat-y ret-quat) (* 0.5 num6))
         (setf (quat-z ret-quat) (* (+ m21 m12) num3))))
      (t
       (let* ((num5 (sqrt (- (1+ m22) m00 m11)))
              (num2 (/ 0.5 num5)))
         (setf (quat-w ret-quat) (* (- m01 m10) num2))
         (setf (quat-x ret-quat) (* (+ m20 m02) num2))
         (setf (quat-y ret-quat) (* (+ m21 m12) num2))
         (setf (quat-z ret-quat) (* 0.5 num5)))))
    ret-quat))

(defun quat-from-euler (roll pitch yaw)
  (declare (type real roll pitch yaw))
  (let ((t0 (cos (* yaw 0.5)))
        (t1 (sin (* yaw 0.5)))
        (t2 (cos (* roll 0.5)))
        (t3 (sin (* roll 0.5)))
        (t4 (cos (* pitch 0.5)))
        (t5 (sin (* pitch 0.5))))
    (make-instance
     'quaternion
     :w (+ (* t0 t2 t4) (* t1 t3 t5))
     :x (- (* t0 t3 t4) (* t1 t2 t5))
     :y (+ (* t0 t2 t5) (* t1 t3 t4))
     :z (- (* t1 t2 t4) (* t0 t3 t5)))))

(defun quat-to-euler (q
                      &aux (ysqr (expt (quat-y q) 2)))
  (declare (type quaternion q))
  (let ((w (quat-w q))
        (x (quat-x q))
        (y (quat-y q))
        (z (quat-z q)))
    (values
     (let ((t0 (* 2 (+ (* w x) (* y z))))
           (t1 (- 1 (* 2 (+ (* x x) ysqr)))))
       (atan t0 t1))
     (let ((t2 (max
                -1
                (min 1
                     (* 2 (- (* w y) (* z x)))))))
       (asin t2))
     (let ((t3 (* 2 (+ (* w z) (* x y))))
           (t4 (- 1 (* 2 (+ ysqr (* z z))))))
       (atan t3 t4)))))

(defun quat-rot-vec (q v)
  (declare (type quaternion q)
           (vec3 v))
  (let* ((u (%v q))
         (s (quat-w q))
         (u-cross-v (vec-cross u v)))
    (vec+ (vec* u 2 (vec-dot u v))
          (vec* v (- (expt s 2) (vec-dot u u)))
          (vec* u-cross-v 2 s))))
;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:defpackage+-user-1)

(defpackage+ #:luna.math
  (:use
   #:cl)
  (:local-nicknames
   (#:serial #:luna.serial))
  (:export
   #:+vec-zero+
   #:+vec-right+
   #:+vec-up+
   #:+vec-forward+
   #:vec3
   #:vec-x
   #:vec-y
   #:vec-z
   #:vec+
   #:vec-
   #:vec*
   #:vec/
   #:vec-cross
   #:vec-dot
   #:vec-mag
   #:vec-sqr-mag
   #:vec-dist
   #:vec-sqr-dist
   #:vec-norm
   #:vec-project
   #:vec-no-norm-project

   #:+quat-identity+

   #:quaternion
   #:quat
   #:quat-w
   #:quat-x
   #:quat-y
   #:quat-z
   #:quat+
   #:quat-
   #:quat*
   #:quat/
   #:quat-conj
   #:quat-scale
   #:quat-norm
   #:quat-inverse
   #:quat-dot
   #:quat-lerp
   #:quat-slerp
   #:quat-look-rot
   #:quat-from-rot
   #:quat-from-euler
   #:quat-to-euler
   #:quat-rot-vec))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package :luna.math)

(defclass vec3 (serial:serializable)
  ((x
    :type single-float :initform 0.0
    :initarg :x :accessor vec-x)
   (y
    :type single-float :initform 0.0
    :initarg :y :accessor vec-y)
   (z
    :type single-float :initform 0.0
    :initarg :z :accessor vec-z)))

(defmethod print-object ((v vec3) stream)
  (print-unreadable-object (v stream :type t)
    (format stream
            "(~5,2,,,'0F, ~5,2,,,'0F, ~5,2,,,'0F)"
            (vec-x v)
            (vec-y v)
            (vec-z v))))

(defmethod serial:serialized-slots ((obj vec3))
  '(x y z))

(defun vec3 (&key (x 0.0) (y 0.0) (z 0.0))
  (declare (type real x y z))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (make-instance 'vec3 :x (float x single-float-epsilon) :y (float y single-float-epsilon) :z (float z single-float-epsilon)))

(defparameter +vec-zero+ (vec3))
(defparameter +vec-up+ (vec3 :y -1.0))
(defparameter +vec-forward+ (vec3 :z -1.0))
(defparameter +vec-right+ (vec3 :x 1.0))

(defun vec+ (v &rest quantities)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((res-x (vec-x v))
        (res-y (vec-y v))
        (res-z (vec-z v)))
    (declare (type single-float res-x res-y res-z))

    (dolist (q quantities)
      (etypecase q
        (real
         (setf q (float q single-float-epsilon))
         (incf res-x q)
         (incf res-y q)
         (incf res-z q))
        (vec3
         (with-slots (x y z) q
           (declare (type single-float x y z))
           (incf res-x x)
           (incf res-y y)
           (incf res-z z)))))

    (vec3 :x res-x :y res-y :z res-z)))

(defun vec- (v &rest quantities)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  ;;Special case if no quantities to subtract, negate
  (if (null quantities)
      (vec3
       :x (- (the single-float (vec-x v)))
       :y (- (the single-float (vec-y v)))
       :z (- (the single-float (vec-z v))))
      ;;Otherwise do subtraction as normal
      (let ((res-x (vec-x v))
            (res-y (vec-y v))
            (res-z (vec-z v)))
        (declare (type single-float res-x res-y res-z))

        (dolist (q quantities)
          (etypecase q
            (real
             (setf q (float q single-float-epsilon))
             (decf res-x q)
             (decf res-y q)
             (decf res-z q))
            (vec3
             (with-slots (x y z) q
               (declare (type single-float x y z))
               (decf res-x x)
               (decf res-y y)
               (decf res-z z)))))

        (vec3 :x res-x :y res-y :z res-z))))

(defun vec* (v &rest numbers)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((res-x (vec-x v))
        (res-y (vec-y v))
        (res-z (vec-z v)))
    (declare (type single-float res-x res-y res-z))

    (macrolet ((incf* (place amt)
                 `(setf ,place (* ,place ,amt))))
      (dolist (q numbers)
        (etypecase q
          (real
           (setf q (float q single-float-epsilon))
           (incf* res-x q)
           (incf* res-y q)
           (incf* res-z q)))))

    (vec3 :x res-x :y res-y :z res-z)))

(defun vec/ (v n &rest numbers)
  (declare (type vec3 v)
           (type real n))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (setf n (float n single-float-epsilon))
  (let ((res-x (/ (the single-float (vec-x v)) n))
        (res-y (/ (the single-float (vec-y v)) n))
        (res-z (/ (the single-float (vec-z v)) n)))
    (declare (type single-float res-x res-y res-z))

    (macrolet ((incf/ (place amt)
                 `(setf ,place (/ ,place ,amt))))
      (dolist (q numbers)
        (etypecase q
          (real
           (setf q (float q single-float-epsilon))
           (incf/ res-x q)
           (incf/ res-y q)
           (incf/ res-z q)))))

    (vec3 :x res-x :y res-y :z res-z)))

(defun vec-cross (v1 v2 &rest vectors)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((res
         (with-slots ((x1 x) (y1 y) (z1 z)) v1
           (declare (type single-float x1 y1 z1))
           (with-slots ((x2 x) (y2 y) (z2 z)) v2
             (declare (type single-float x2 y2 z2))
             (vec3
              :x (- (* y1 z2) (* z1 y2))
              :y (- (* z1 x2) (* x1 z2))
              :z (- (* x1 y2) (* y1 x2)))))))
    (with-slots ((x1 x) (y1 y) (z1 z)) res
      (declare (type single-float x1 y1 z1))
      (dolist (v vectors)
        (declare (type vec3 v))
        (with-slots ((x2 x) (y2 y) (z2 z)) v
          (declare (type single-float x2 y2 z2))
          (psetf x1 (- (* y1 z2) (* z1 y2))
                 y1 (- (* z1 x2) (* x1 z2))
                 z1 (- (* x1 y2) (* y1 x2))))))
    res))

(defun vec-dot (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (with-slots ((x1 x) (y1 y) (z1 z)) v1
    (declare (type single-float x1 y1 z1))
    (with-slots ((x2 x) (y2 y) (z2 z)) v2
      (declare (type single-float x2 y2 z2))
      (+ (* x1 x2)
         (* y1 y2)
         (* z1 z2)))))

(defun vec-sqr-mag (v)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (with-slots (x y z) v
    (declare (type single-float x y z))
    (+ (* x x) (* y y) (* z z))))

(defun vec-mag (v)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (the single-float (sqrt (vec-sqr-mag v))))

(defun vec-sqr-dist (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (space 0) (safety 0) (debug 0) (compilation-speed 0)))
  (with-slots ((x1 x) (y1 y) (z1 z)) v1
    (with-slots ((x2 x) (y2 y) (z2 z)) v2
      (declare (type single-float x1 y1 z1 x2 y2 z2))
      (let ((dx (- x1 x2))
            (dy (- y1 y2))
            (dz (- z1 z2)))
        (declare (type single-float dx dy dz))
        (+ (* dx dx)
           (* dy dy)
           (* dz dz))))))

(defun vec-dist (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))
  (the single-float (sqrt (vec-sqr-dist v1 v2))))

(defun vec-norm (v)
  (declare (type vec3 v))
  (declare (optimize (speed 3) (space 0) (safety 0) (debug 0) (compilation-speed 0)))
  (let ((length (vec-sqr-mag v)))
    (if (zerop length)
        (vec3)
        (vec/ v (the single-float (sqrt length))))))

(defun vec-no-norm-project (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (space 0) (safety 0) (debug 0) (compilation-speed 0)))
    (with-slots ((x1 x) (y1 y) (z1 z)) v1
    (declare (type single-float x1 y1 z1))
    (with-slots ((x2 x) (y2 y) (z2 z)) v2
      (declare (type single-float x2 y2 z2))
      (let ((len (+ (* x1 x2)
                    (* y1 y2)
                    (* z1 z2))))
        (vec3
         :x (* len x1)
         :y (* len y1)
         :z (* len z1))))))

(defun vec-project (v1 v2)
  (declare (type vec3 v1 v2))
  (declare (optimize (speed 3) (space 0) (safety 0) (debug 0) (compilation-speed 0)))
  (with-slots ((x1 x) (y1 y) (z1 z)) v1
    (declare (type single-float x1 y1 z1))
    (with-slots ((x2 x) (y2 y) (z2 z)) v2
      (declare (type single-float x2 y2 z2))
      (let ((len (/ (+ (* x1 x2)
                       (* y1 y2)
                       (* z1 z2))
                    (the single-float
                         (sqrt
                          (+ (* x1 x1)
                             (* y1 y1)
                             (* z1 z1)))))))
        (vec3
         :x (* len x1)
         :y (* len y1)
         :z (* len z1))))))
;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:luna.physics)

(defun get-interval (axis p)
  (let* ((d (vec-dot axis (poly-vertex p 0)))
         (min d)
         (max d))
    (loop
       :for vertex :across (poly-vertices p)
       :for d := (vec-dot vertex axis)
       :if (< d min)
       :do (setf min d)
       :else :if (> d max)
       :do (setf max d))
    (values min max)))

(defun interval-intersect (a b axis offset)
  (multiple-value-bind (min0 max0) (get-interval axis a)
    (multiple-value-bind (min1 max1) (get-interval axis b)
      (let ((h (vec-dot offset axis)))
        (incf min0 h)
        (incf max0 h))
      (and (<= (- min0 max1) 0)
           (<= (- min1 max0) 0)))))

(defun collide (a b offset
                &aux
                  (tested-axes
                   (make-array +poly-max-vertices+))
                  (num-axes 0))
  (declare (dynamic-extent tested-axes))

  (loop
     :for edge :across (poly-edges a)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :do (setf (svref tested-axes num-axes) axis)
     :unless (interval-intersect a b axis offset)
     :do (return-from collide nil)
     :else
     :do (incf num-axes))

  (loop
     :for edge :across (poly-edges b)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :do (setf (svref tested-axes num-axes) axis)
     :do (incf num-axes)
     :unless (interval-intersect a b axis offset)
     :do (return-from collide nil)
     :else
     :do (incf num-axes))
  t)

(defun interval-intersect-td (a b axis offset)
  (multiple-value-bind (min0 max0) (get-interval axis a)
    (multiple-value-bind (min1 max1) (get-interval axis b)
      (let ((h (vec-dot offset axis)))
        (incf min0 h)
        (incf max0 h))
      (let ((d0 (- min0 max1))
            (d1 (- min1 max0)))
        (if (and (<= d0 0)
                 (<= d1 0))
            (max d0 d1)
            nil)))))

(defun find-mtd (axes tds num-axes)
  (loop
     :with mini := nil
     :with smallest-td := 0
     :for i :below num-axes
     :for axis := (svref axes i)
     :for td := (svref tds i)
     :for len := (/ td (vec-mag axis))
     :if (or (> len smallest-td)
             (null mini))
     :do
     (setf smallest-td len)
     (setf mini (vec* (vec-norm axis) len))
     :finally
     (unless mini
       (error "find-mtd: failed"))
     (return (values mini smallest-td))))

(defun collide-mtd (a b offset
                    &aux
                      (tested-axes
                       (make-array +poly-max-vertices+))
                      (tested-axes-translations
                       (make-array +poly-max-vertices+))
                      (num-axes 0))
  (declare (dynamic-extent tested-axes tested-axes-translations))

  (loop
     :for edge :across (poly-edges a)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :for td := (interval-intersect-td a b axis offset)
     :unless td
     :do (return-from collide-mtd (values nil nil))
     :else
     :do
     (setf (svref tested-axes num-axes) axis)
     (setf (svref tested-axes-translations num-axes) td)
     (incf num-axes))

  (loop
     :for edge :across (poly-edges b)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :for td := (interval-intersect-td a b axis offset)
     :unless td
     :do (return-from collide-mtd (values nil nil))
     :else
     :do
     (setf (svref tested-axes num-axes) axis)
     (setf (svref tested-axes-translations num-axes) td)
     (incf num-axes))

  (multiple-value-bind (mtd-dir td)
      (find-mtd tested-axes tested-axes-translations num-axes)
    (when (minusp (vec-dot mtd-dir offset))
      (setf mtd-dir (vec- mtd-dir)))
    (values mtd-dir td)))

(defun interval-intersect-vel-td (a b axis offset vel dt
                                  &aux (v (vec-dot vel axis)))
  (multiple-value-bind (min0 max0) (get-interval axis a)
    (multiple-value-bind (min1 max1) (get-interval axis b)
      (let ((h (vec-dot offset axis)))
        (incf min0 h)
        (incf max0 h))
      (let ((d0 (- min0 max1))
            (d1 (- min1 max0)))
        (cond
          ((and (< d0 0)
                (< d1 0))
           ;;Overlapped, return amount
           (if (> d0 d1) d0 d1))
          ((zerop v)
           ;;Small velocity, and no overlap
           ;;No intersection
           nil)
          (t
           ;;Separated right now, test future
           (let ((t0 (- (/ d0 v)))  ;time of impact to d0 reaches 0
                 (t1 (+ (/ d1 v)))) ;time of impact to d0 reaches 1
             (when (> t0 t1) ;sort t0 < t1
               (rotatef t0 t1))

             (let ((time-to-hit (if (> t0 0) t0 t1)))
               (if (<= 0 time-to-hit dt)
                   time-to-hit
                   nil)))))))))

(defun find-vel-mtd (axes tds num-axes
                     &aux
                       (best-t 0)
                       (mini nil))
  ;;find collision first
  (loop
     :for i :below num-axes
     :for td := (svref tds i)
     :if (and (> td 0)
              (> td best-t))
     :do
     (setf best-t td)
     (setf mini (svref axes i)))

  (when mini
    (return-from find-vel-mtd
      (values (vec-norm mini) best-t)))

  ;;otherwise try and find overlaps
  (loop
     :for i :below num-axes
     :for axis := (svref axes i)
     :for td := (svref tds i)
     :for len := (/ td (vec-mag axis))
     :if (or (> len best-t)
             (null mini))
     :do
     (setf best-t len)
     (setf mini axis))

  (unless mini
    (error "find-mtd: failed"))

  (values (vec-norm mini) best-t))

(defun collide-vel-mtd (a b offset vel dt
                        &aux
                          (tested-axes
                           (make-array +poly-max-vertices+))
                          (tested-axes-translations
                           (make-array +poly-max-vertices+))
                          (num-axes 0))
  (when (> (vec-sqr-mag vel) double-float-epsilon)
    (let ((axis (vec3 :x (- (vec-z vel)) :z (vec-x vel))))
      (setf (svref tested-axes num-axes) axis)
      (let ((td (interval-intersect-vel-td a b axis offset vel dt)))
        (unless td
          (return-from collide-vel-mtd (values nil nil)))
        (setf (svref tested-axes-translations num-axes) td))))

  (loop
     :for edge :across (poly-edges a)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :for td := (interval-intersect-vel-td a b axis offset vel dt)
     :unless td
     :do (return-from collide-vel-mtd (values nil nil))
     :else
     :do
     (setf (svref tested-axes num-axes) axis)
     (setf (svref tested-axes-translations num-axes) td)
     (incf num-axes))

  (loop
     :for edge :across (poly-edges b)
     :for axis := (vec3 :x (- (vec-z edge)) :z (vec-x edge))
     :for td := (interval-intersect-vel-td a b axis offset vel dt)
     :unless td
     :do (return-from collide-vel-mtd (values nil nil))
     :else
     :do
     (setf (svref tested-axes num-axes) axis)
     (setf (svref tested-axes-translations num-axes) td)
     (incf num-axes))

  (multiple-value-bind (mtd-dir td)
      (find-vel-mtd tested-axes tested-axes-translations num-axes)
    (when (minusp (vec-dot (vec-norm mtd-dir) offset))
      (setf mtd-dir (vec- mtd-dir)))
    (values mtd-dir td)))

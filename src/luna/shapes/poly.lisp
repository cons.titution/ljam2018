;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:luna.shapes)

(defconstant +poly-max-vertices+ 32)

(defclass poly (serial:serializable)
  ((vertices
    :type (simple-array vec3 (*))
    :reader poly-vertices)))

(defmethod initialize-instance :after ((poly poly)
                                       &key (vertices ())
                                         &allow-other-keys)
  (setf (slot-value poly 'vertices)
        (map 'simple-vector #'vec+ vertices)))

(defmethod serial:serialized-slots ((obj poly))
  '(vertices))

(defun poly-vertex (poly index)
  (svref (poly-vertices poly) index))

(defun poly-num-vertices (poly)
  (length (poly-vertices poly)))

(defun poly-edge (poly index)
  (if (= index (1- (poly-num-vertices poly)))
      (vec- (poly-vertex poly 0) (poly-vertex poly index))
      (vec- (poly-vertex poly (1+ index)) (poly-vertex poly index))))

(defun poly-edges (poly
                   &aux
                     (num-vertices (poly-num-vertices poly)))
  (let ((ret (make-array num-vertices :element-type 'vec3 :initial-element (vec3))))
    (loop
       :for i :below num-vertices
       :do (setf (svref ret i) (poly-edge poly i)))
    ret))

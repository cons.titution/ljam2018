(in-package #:ljam2018)

(defclass game-app (app)
  ((window
    :type ui:window
    :initform nil
    :accessor window)
   (gameplay
    :initform nil
    :accessor gameplay)))

(defmethod app-init ((app game-app))
  ;;Init debug
  (debug-init)

  ;;Create our window
  (setf (window app)
        (make-instance
         'ui:window
         :title "Deathpong"
         ;;TODO hack to make the map a multiple of 32
         :width 800 :height 608
         :content (make-instance 'main-menu :app app))))

(defmethod app-uninit ((app game-app))
  (ui:window-close (window app))

  (setf (window app) nil)

  (debug-uninit))

(defun %enter-main-menu (app)
  (setf (ui:content (window app)) (make-instance 'main-menu :app app)))

(defun %leave-main-menu (app)
  (setf (ui:content (window app)) nil))

(defun %enter-gameplay (app)
  (let ((gameplay (make-instance 'gameplay :app app)))
    (setf (gameplay app) gameplay
          (ui:content (window app)) gameplay)))

(defun %leave-gameplay (app)
  (setf (gameplay app) nil)
  (setf (ui:content (window app)) nil))

(defun main ()
  #+linux (setf sol.media::*default-font-family* "Overpass Mono")
  (uiop:with-current-directory
      ((uiop:pathname-parent-directory-pathname
        (uiop:pathname-parent-directory-pathname
         (asdf:system-source-directory :ljam2018))))
    (app-start 'game-app)))

(defun main-exe ()
  ;;TODO hacky hack for making sure we're in the right dir from an executable
  (uiop:with-current-directory
      ((uiop:pathname-directory-pathname sb-ext:*core-pathname*))
    (app-start 'game-app)))

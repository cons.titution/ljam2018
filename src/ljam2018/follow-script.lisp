(in-package #:ljam2018)

(define-component follow-script (script)
  ((target
    :type (or null ecs:entity)
    :initarg :target
    :accessor target
    :property-p t)
   (offset
    :type vec3
    :initarg :offset
    :initform (vec3)
    :accessor offset
    :property-p t)))

(defmethod update ((comp follow-script))
  (when (target comp)
    (ecs:with-components (script-entity comp)
        ((t1 transform))
      (ecs:with-components (target comp)
          ((t2 transform))
        (setf (location t1) (vec+ (offset comp) (location t2)))))))

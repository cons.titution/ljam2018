(in-package :ljam2018)

(defgeneric lob (script kind)
  (:documentation "potion effect when thrown for the given potion-kind"))

(defgeneric drink (script kind)
  (:documentation "potion effect when drunk for the given potion-kind"))


(defun %make-lob (kind script thunk)
  (let* ((player (script-entity script))
         (transform (get-component player 'transform))
         (lobdir (vec3 :x (cos (rotation transform))
                       :z (sin (rotation transform))))
         (lobspeed 100)
         (lobfar (vec* lobdir 50))
         (lobplace (vec+ lobfar (location transform)))
         (dummypot (create-entity :name "thrown-pot")))
    (mapc (lambda (c) (ecs:add-component dummypot c))
        (list
         (make-instance 'sprite :path (pickup-image kind))
         (make-instance 'transform :location (location transform))
         (make-instance 'rigid-body :velocity (vec* lobdir lobspeed))
         (make-instance 'rect-collider :width 35 :height 45 :trigger-p t)
         (make-instance 'potion-lob-script
                        :target lobplace
                        :thunk thunk)))))

(defmethod drink ((script deathpong-player) (kind (eql nil))) nil)
(defmethod lob ((script deathpong-player) (kind (eql nil))) nil)

(defmethod drink ((script deathpong-player) (kind (eql :wall)))
  (dtext (200 200) "Drank wall!")
  (let ((player (script-entity script)))
    (setf (getf (attributes player) :wall) t)
    (%do-after 1
      (setf (getf (attributes player) :wall) nil))))

(defun %make-wall (pos)
  (let ((wall (create-entity :name "Wall" :attributes '(:wall t))))
    (mapc (lambda (c) (ecs:add-component wall c))
          (list
           (make-instance 'sprite :path "data/images/pong/wall.png")
           (make-instance 'transform :location pos)
           (make-instance 'rigid-body)
           (make-instance 'rect-collider :width 16 :height 16)))))

(defmethod lob ((script deathpong-player) (kind (eql :wall)))
  (dtext (200 200) "Lobbed wall!")
  (%make-lob :wall script '%make-wall))

(defmethod pickup-image ((item (eql :wall)))
  #P"data/images/pong/pot1.png")

(in-package #:defpackage+-user-1)

(defpackage+ #:ljam2018
  (:use
   #:alexandria
   #:cl
   #:sol
   #:luna.ecs
   #:luna.math
   #:mar.components)
  (:local-nicknames
   (#:dispatcher #:sol.dispatcher)
   (#:input #:sol.input)
   (#:media #:sol.media)
   (#:media.colors #:sol.media.colors)
   (#:ui #:sol.ui)
   (#:control #:luna.control)
   (#:ecs #:luna.ecs)
   (#:math #:luna.math)
   (#:serial #:luna.serial)
   (#:components #:mar.components)
   (#:sdl2 #:raw-bindings-sdl2))
  (:export
   #:main))

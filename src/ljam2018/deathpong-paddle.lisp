(in-package :ljam2018)

(define-component paddle-script (script)
  ((balls :type (array entity *)
          :initarg :balls
          :initform #()
          :accessor balls)
   (maxspeed :type real
             :initarg :maxspeed
             :initform 420.0
             :accessor maxspeed)))

(defmethod fixed-update ((script paddle-script) dt)
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body))
    (let* ((balls (balls script))
           (maxspeed (maxspeed script))
           (here (location transform))
           (closest (reduce
                     (lambda (a n)
                       (let* ((prev (location a))
                              (there (location n))
                              (tothere (vec-mag (vec- here there)))
                              (toprev (vec-mag (vec- here prev))))
                         (if (< tothere toprev) n a)))
                     balls)))
      (let ((my-z (vec-z (location transform)))
            (ball-z (vec-z (location (ecs:get-component closest 'transform)))))
        (setf (velocity bod) (vec3 :z (clamp (* 2 (- ball-z my-z))
                                             (- maxspeed)
                                             maxspeed)))))))

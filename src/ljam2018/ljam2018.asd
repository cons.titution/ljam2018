(defsystem #:ljam2018
  :name "ljam2018"
  :version "0.0.0.0"
  :description "The game. Lose it"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :components
  ((:file "package")
   (:file "util")
   (:file "debug")
   (:file "fps-calculator")
   (:file "gameplay")
   (:file "main-menu")
   (:file "main")
   (:file "pace-script")
   (:file "ballscript")
   (:file "follow-script")
   (:file "potion-lob-script")
   (:file "pickup")
   (:file "deathpong-player")
   (:file "deathpong-paddle")
   (:file "deathpong-potions")
   (:file "deathpong-controller"))
  :depends-on
  (#:alexandria
   #:defpackage-plus
   #:sol
   #:sol.sdl2-driver
   #:luna
   #:mar
   #:raw-bindings-sdl2))

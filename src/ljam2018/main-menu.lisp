(in-package #:ljam2018)

(defclass main-menu (ui:content-control)
  ((app
    :type game-app
    :initarg :app
    :initform (error "main-menu: must provide a app")
    :reader app))
  (:default-initargs
   :horizontal-content-alignment :stretch
    :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((comp main-menu)
                                       &key &allow-other-keys
                                       &aux (app (app comp)))
  (let ((banner-image
         (make-instance
          'ui:image
          :vertical-alignment :top
          :margin (make-instance 'ui:thickness :top 50)
          :source "data/images/banner/pong.png"
          :stretch :none))
        (start-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :font-size 16
          :content "Start"
          :background +shade-2+
          :clicked-color +shade-3+
          :command
          (make-instance
           'ui:delegate-command
           :execute (lambda (args)
                      (declare (ignore args))
                      (%leave-main-menu app)
                      (%enter-gameplay app)))))
        (exit-button
         (make-instance
          'ui:button
          :width 75 :height 40
          :font-size 16
          :background +shade-2+
          :clicked-color +shade-3+
          :content "Exit"
          :command
          (make-instance
           'ui:delegate-command
           :execute (lambda (args)
                      (declare (ignore args))
                      (ui:window-close (window app)))))))

    (setf (ui:content comp)
          (make-instance
           'ui:grid
           :background +shade-1+
           :children
           (list
            banner-image
            (make-instance
             'ui:stack-panel
             :orientation :vertical
             :spacing 15
             :horizontal-alignment :center
             :vertical-alignment :top
             :margin
             (make-instance 'ui:thickness :left 50 :top 300)
             :children
             (list
              start-button
              exit-button)))))))

(in-package #:ljam2018)

(define-component ball-script (script)
  ((map-width
    :type real
    :initarg :map-width
    :initform (error "ball-script: Must supply a map width")
    :reader map-width
    :property-p t)
   (map-height
    :type real
    :initarg :map-height
    :initform (error "ball-script: Must supply a map height")
    :reader map-height
    :property-p t)
   (ball-score-callback
    :type (or function symbol)
    :initarg :ball-score-callback
    :accessor ball-score-callback)))

(defmethod fixed-update ((script ball-script) dt)
  (declare (ignore dt))
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body))
    (with-accessors ((vec-x vec-x) (vec-z vec-z)) (location transform)
      (dtext (400 50) "~a" vec-z)

      ;;;See if we went past the up/down bounds
      ;; 5 here as a margin
      (when (or (< (- (map-height script) 5.0) vec-z)
                (< vec-z 5.0))
        (setf (velocity bod)
              (vec3 :x (vec-x (velocity bod))
                    :z (- (vec-z (velocity bod))))))

      ;;See if we made it past the 'goal' on the left or right
      (when (or (< (- (map-width script) 5.0) vec-x)
                (< vec-x 5.0))
        (when (ball-score-callback script)
          (funcall (ball-score-callback script) script))))))

(defmethod on-trigger ((script ball-script) col)
  (ecs:with-components (script-entity script)
      ((bod rigid-body))
    (cond ((getf (attributes (component-entity (mar:collision-event-collider col)))
                 :paddle)
           (setf (velocity bod) (vec3 :x (- (vec-x (velocity bod)))
                                      :z (vec-z (velocity bod)))))

          ((getf (attributes (component-entity (mar:collision-event-collider col)))
                 :wall)
           (setf (velocity bod)
                 (vec* (vec-norm (vec+ (vec-norm (velocity bod)) (vec-norm (mar:collision-event-mtd-dir col))))
                       (vec-mag (velocity bod))))))))

(in-package #:ljam2018)

(defparameter *%debug-color* media.colors:*red*)
(defparameter *%draw-debug-text* nil)
(defvar *%debug-font* nil)
(defvar *%tracked-debug-text* (make-hash-table))
(defvar *%untracked-debug-text* nil)

(defclass %debug-text-entry ()
  ((text
    :initarg :text
    :accessor text)
   (x
    :initarg :x
    :accessor x)
   (y
    :initarg :y
    :accessor y)
   (initial-time
    :initarg :time
    :reader initial-time)
   (remaining-time
    :initarg :time
    :accessor remaining-time)))

(defun debug-init ()
  (setf *%debug-font*
        (make-instance 'media:font :size 20))
(values))

(defun debug-render (renderer)
  (unless *%draw-debug-text*
    (return-from debug-render))
  (flet ((draw-entry (entry)
           (let ((x (if (plusp (x entry)) (x entry) (+ (x entry) (media:renderer-width renderer))))
                 (y (if (plusp (y entry)) (y entry) (+ (y entry) (media:renderer-height renderer)))))
             (media:render-draw-text renderer (text entry) x y))))
    (dolist (entry (hash-table-values *%tracked-debug-text*))
      (draw-entry entry))
    (dolist (entry *%untracked-debug-text*)
      (draw-entry entry))))

(defun debug-uninit ()
  (setf *%untracked-debug-text* nil)
  (clrhash *%tracked-debug-text*)
  (setf *%debug-font* nil))

(defun debug-text (text x y &key (token nil) (time 5))
  (if token
      (%ensure-entry token text x y time)
      (setf *%untracked-debug-text*
            (nconc *%untracked-debug-text*
                   (list (%create-entry text x y time))))))

(defmacro dtext ((x y &key (time 5)) format-string &rest format-args)
  (with-gensyms (x-sym y-sym time-sym token)
    `(let ((,x-sym ,x) (,y-sym ,y)
           (,time-sym ,time))
       (debug-text (format nil ,format-string ,@format-args) ,x-sym ,y-sym :token ',token :time ,time-sym))))

(defun %entry-expired (entry)
  (<= (remaining-time entry) 0))

(defun %on-debug-time-tick (dt)
  (flet ((update-entry (entry)
           (decf (remaining-time entry) dt)))
    (maphash-values #'update-entry *%tracked-debug-text*)
    (mapc #'update-entry *%untracked-debug-text*))
  (let ((removed-tracked
          (remove-if-not
           (lambda (cons)
             (%entry-expired (cdr cons)))
           (hash-table-alist *%tracked-debug-text*))))
    (dolist (cons removed-tracked)
      (remhash (car cons) *%tracked-debug-text*)))

  (let ((removed-untracked
         (remove-if-not #'%entry-expired *%untracked-debug-text*)))
    (setf *%untracked-debug-text* (delete-if #'%entry-expired *%untracked-debug-text*))))

(defun %create-entry (text x y time)
  (let ((entry
         (make-instance
          '%debug-text-entry
          :text (make-instance
                 'media:text
                 :color *%debug-color*
                 :font *%debug-font*
                 :string text)
          :x x
          :y y
          :time time)))
    entry))

(defun %ensure-entry (token text x y time)
  (let ((entry (gethash token *%tracked-debug-text*)))
    (cond
      ((null entry)
       (setf entry (%create-entry text x y time))
       (setf (gethash token *%tracked-debug-text*) entry))
      (t
       (setf (media:text-string (text entry))  text)
       (setf (x entry) x)
       (setf (y entry) y)
       (setf (remaining-time entry) time)))
    entry))

(in-package #:ljam2018)

(defclass gameplay (ui:content-control)
  ()
  (:default-initargs
   :background media.colors:*black*
   :horizontal-content-alignment :stretch
   :vertical-content-alignment :stretch))

(defmethod initialize-instance :after ((gameplay gameplay) &key)
  (let* ((game-ui
           (make-instance
            'ui:content-control
            :horizontal-content-alignment :stretch
            :vertical-content-alignment :stretch))
         (initial-scene
           (make-instance
            'scene
            :name "Deathpong"
            :entities.components
            (list
             (cons
              (make-instance 'ecs:entity :name "Deathpong Controller")
              (list
               (make-instance 'deathpong-controller :ui-control game-ui))))))
         (game-control (make-instance 'game-control :initial-scene initial-scene)))
    (setf (ui:content gameplay)
          (make-instance 'ui:grid :children (list game-control game-ui)))))

(defmethod ui:draw ((comp gameplay) renderer)
  (call-next-method)
  ;;Draw the debug stuff on top of any UI
  (debug-render renderer))

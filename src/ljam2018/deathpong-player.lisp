(in-package #:ljam2018)

(defmacro %with-inputs (input-states &body body)
  (with-gensyms (states input-or-default)
    `(let ((,states ,input-states))
       (labels ((,input-or-default (name fn &optional default)
                  (if-let ((input (get-input ,states name)))
                    (funcall fn input)
                    default))
                (was-pressed (name)
                  (,input-or-default name #'input-was-pressed))
                (is-held (name)
                  (,input-or-default name #'input-is-held))
                (time-since-pressed (name)
                  (,input-or-default name #'input-time-since-pressed))
                (was-released (name)
                  (,input-or-default name #'input-was-released))
                (is-released (name)
                  (,input-or-default name #'input-is-held))
                (time-since-released (name)
                  (,input-or-default name #'input-time-since-released))
                (value (name)
                  (,input-or-default name #'input-value)))
         (declare (ignorable #'was-pressed #'is-held #'time-since-pressed #'was-released #'is-released #'time-since-released #'value))
         ,@body))))

(defclass action-state ()
  ((name
    :type keyword
    :initarg :name
    :initform (error "action-state: must supply name")
    :reader name)
   (animation
    :type keyword
    :initarg :animation
    :initform (error "action-state: must supply animation")
    :reader animation)))

(defgeneric update-action-state (state script dt)
  (:method (state script dt)
    (declare (ignore state script dt))))

(defclass timed-state (action-state)
  ((duration
    :type real
    :initarg :duration
    :initform (error "timed-state: must supply duration")
    :reader duration)
   (elapsed-time
    :type real
    :initform 0
    :accessor elapsed-time)))

(defmethod update-action-state :before ((state timed-state) script dt)
  (declare (ignore script))
  (incf (elapsed-time state) dt))

(defclass idle (action-state)
  ()
  (:default-initargs
   :name :idle
    :animation :idle))

(defmethod update-action-state ((state idle) script dt
                                &aux (movement-dir (movement-dir script)))
  (with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) +vec-zero+)

    (%with-inputs states
      ;;backstep
      (cond
        ((> (vec-mag movement-dir) 0)
         (setf (active-state script) (make-instance 'walk)))
        ((and
          (was-pressed :backstep)
          (<= (time-since-pressed :backstep) 0.25))
         (setf (active-state script)
               (make-instance
                'backstep
                :direction
                (vec-norm
                 (vec3
                  :x (- (cos (rotation transform)))
                  :z (- (sin (rotation transform))))))))))))

(defclass walk (action-state)
  ()
  (:default-initargs
   :name :walk
    :animation :move))

(defmethod update-action-state ((state walk) script dt
                                &aux
                                  (movement-dir (movement-dir script)))
  (with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) (vec* (movement-dir script) (movement-speed script)))
    (unless (zerop (vec-mag (velocity bod)))
      (setf (rotation transform)
            (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))

    (%with-inputs states
      (cond
        ((zerop (vec-mag movement-dir))
         (setf (active-state script) (make-instance 'idle)))
        ((and
          (is-held :run)
          (>= (time-since-pressed :run) 0.25)
          (<= (time-since-pressed :run) 0.50))
         (setf (active-state script) (make-instance 'run)))
        ((and
          (was-released :dash)
          (<= (time-since-pressed :dash) 0.25))
         (setf (active-state script)
               (make-instance 'dash :direction movement-dir)))
        ((and
          (was-released :roll)
          (<= (time-since-pressed :roll) 0.25))
         (setf (active-state script)
               (make-instance 'roll :direction (vec-norm movement-dir))))))))

(defclass run (action-state)
  ()
  (:default-initargs
   :name :run
    :animation :run))

(defmethod update-action-state ((state run) script dt
                                &aux
                                  (movement-dir (movement-dir script)))
  (with-components (script-entity script)
      ((states input-states)
       (transform transform)
       (bod rigid-body))
    (setf (velocity bod) (vec* (movement-dir script) (movement-speed script) 2.5))
    (unless (zerop (vec-mag (velocity bod)))
      (setf (rotation transform)
            (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))

    (%with-inputs states
      (cond
        ((zerop (vec-mag movement-dir))
         (setf (active-state script) (make-instance 'idle)))
        ((not (is-held :run))
         (setf (active-state script) (make-instance 'walk)))))))

(defclass dash (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "dash: must supply direction")
    :reader direction))
  (:default-initargs
   :name :dash
    :animation :move
    :duration 0.075))

(defmethod update-action-state ((state dash) script dt)
  (with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 4.0))
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass backstep (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "backstep: must supply direction")
    :reader direction))
  (:default-initargs
   :name :backstep
    :animation :move
    :duration 0.125))

(defmethod update-action-state ((state backstep) script dt)
  (with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 3.0))
    (when (>= (elapsed-time state) (duration state))
      (setf (active-state script) (make-instance 'post-backstep)))))

(defclass post-backstep (timed-state)
  ()
  (:default-initargs
   :name :post-backstep
    :animation :post-backstep
    :duration 0.25))

(defmethod update-action-state ((state post-backstep) script dt)
  (with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(defclass roll (timed-state)
  ((direction
    :type vec3
    :initarg :direction
    :initform (error "roll: must supply direction")
    :reader direction))
  (:default-initargs
   :name :roll
    :animation :roll
    :duration 0.45))

(defmethod update-action-state ((state roll) script dt)
  (with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) (vec* (direction state) (movement-speed script) 2.5))
    (when (>= (elapsed-time state) (duration state))
      (setf (active-state script) (make-instance 'post-roll)))))

(defclass post-roll (timed-state)
  ()
  (:default-initargs
   :name :post-roll
    :animation :post-roll
    :duration 0.25))

(defmethod update-action-state ((state post-roll) script dt)
  (with-components (script-entity script)
      ((bod rigid-body))
    (setf (velocity bod) +vec-zero+)
    (when (>= (elapsed-time state) (duration state))
      (cond
        ((zerop (vec-mag (movement-dir script)))
         (setf (active-state script) (make-instance 'idle)))
        (t
         (setf (active-state script) (make-instance 'walk)))))))

(define-component deathpong-player (script)
  ((movement-speed
    :type real
    :initarg :movement-speed
    :initform 0
    :accessor movement-speed
    :property-p t)
   (movement-dir
    :type vec3
    :initform +vec-zero+
    :accessor movement-dir
    :property-p t)
   (active-state
    :type action-state
    :initform (make-instance 'idle)
    :accessor active-state)
   (inventory
    :type list
    :initform nil
    :accessor inventory
    :property-p t)))

(defun %player-on-pickup (script pickup)
  (when (on-player-pickup script (payload pickup))
    (destroy-entity (script-entity pickup))))

(defgeneric on-player-pickup (script pickup-contents)
  (:method ((script deathpong-player) payload)
    (warn "Unhandled pickup: ~A x ~A" script payload)))

(defmethod component-init ((script deathpong-player) entity)
  (call-next-method)
  (subscribe-event (entity-events entity) 'on-pickup script '%player-on-pickup))

(defmethod component-uninit ((script deathpong-player) entity)
  (unsubscribe-event (entity-events entity) 'on-pickup script))

(defmethod fixed-update ((script deathpong-player) dt)
  (with-components (script-entity script)
      ((states input-states))
    (%with-inputs states
      ;;Handle movement input
      (with-slots (movement-dir) script

        (when (was-pressed :lob)
          (lob script (pop (inventory script))))

        (setf movement-dir +vec-zero+)
        (when (is-held :left)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-right+ (value :left)))))

        (when (is-held :up)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-forward+ (value :up)))))

        (when (is-held :right)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-right+ (value :right)))))

        (when (is-held :down)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-forward+ (value :down)))))

        (when (is-held :left/right)
          (setf movement-dir
                (vec+ movement-dir (vec* +vec-right+ (value :left/right)))))

        (when (is-held :up/down)
          (setf movement-dir
                (vec- movement-dir (vec* +vec-forward+ (value :up/down)))))

        (when (was-pressed :drink)
          (drink script (pop (inventory script))))

        (when (> (vec-mag movement-dir) 1)
          (setf movement-dir (vec-norm movement-dir))))))
  (update-action-state (active-state script) script dt))

(defmethod update ((script deathpong-player))
  (with-components (script-entity script)
      ((transform transform)
       (bod rigid-body)
       (sprite sprite))

    (let* ((direction (%rotation-to-dir (rotation transform))))
      (dtext (5 -90) "state: ~A" (name (active-state script)))
      (dtext (5 -70) "rotation: ~5,2,,,'0F" (rotation transform))
      (dtext (5 -50) "location: ~A" (location transform))

      (let ((animation
             (make-keyword (format nil "~A-~A" (animation (active-state script)) direction))))
        (dtext (5 -30) "animation: ~A" animation)
        (setf (current-animation sprite) animation)))))

(defmethod on-collision ((script deathpong-player) col)
  (dtext (5 -140 :time 1) "Collided with: ~A" col))

(defmethod on-trigger ((script deathpong-player) col)
  (dtext (5 -120 :time 1) "Triggered on: ~A" col))

(defun %rotation-to-dir (rotation)
  (let ((x (cos rotation))
        (y (sin rotation)))
    (if (>= (abs x) (abs y))
        (if (plusp x) :right :left)
        (if (plusp y) :down :up))))

(defmethod on-player-pickup ((script deathpong-player) (potion-type symbol))
  (when (inventory script)
    ;;Chop off the second item in the list and add us in
    (setf (cdr (inventory script)) nil))
  (push potion-type (inventory script))
  ;;Return t to verify that we picked it up
  t)

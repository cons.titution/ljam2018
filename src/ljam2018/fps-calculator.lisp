(in-package #:ljam2018)

(defclass fps-calculator ()
  ((fps
    :type real
    :initform 0
    :accessor fps)
   (samples
    :type simple-vector
    :initform (make-array 60 :element-type 'real :initial-element 0)
    :reader %fps-samples)
   (available-samples
    :type integer
    :initform 0
    :accessor %fps-available-samples)
   (samples-index
    :type integer
    :initform 0
    :accessor %fps-samples-index)
   (samples-sum
    :type real
    :initform 0
    :accessor %fps-samples-sum)
   (frames-since-sample
    :type integer
    :initform 0
    :accessor %frames-since-sample)
   (time-since-sample
    :type real
    :initform 0
    :accessor %time-since-sample)))

(defun fps-time-tick (fps dt)
  (incf (%time-since-sample fps) dt)

  (when (>= (%time-since-sample fps) .1)
    (setf (fps fps)
          (%fps-add-fps-sample fps (/ (%frames-since-sample fps)
                                      (%time-since-sample fps))))
    (setf (%frames-since-sample fps) 0)
    (setf (%time-since-sample fps) 0))
  (values))

(defun fps-frame-tick (fps)
  (incf (%frames-since-sample fps))
  (values))

(defun %fps-add-fps-sample (fps fps-sample)
  (with-slots (samples samples-index available-samples samples-sum)
      fps
    (decf samples-sum (svref samples samples-index))
    (incf samples-sum fps-sample)
    (setf (svref samples samples-index) fps-sample)
    (setf available-samples (min (1+ available-samples)
                                      (length samples)))
    (when (= (incf samples-index) (length samples))
      (setf samples-index 0))

    (/ samples-sum available-samples)))

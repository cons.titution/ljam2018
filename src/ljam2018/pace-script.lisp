(in-package #:ljam2018)

(define-component pace-script (script)
  ((movement-speed
    :type real
    :initarg :movement-speed
    :initform 0
    :accessor movement-speed
    :property-p t)
   (p1
    :type vec3
    :initform (vec3)
    :initarg :p1
    :accessor p1
    :property-p t)
   (p2
    :type vec3
    :initform (vec3)
    :initarg :p2
    :accessor p2
    :property-p t)
   (toggle
    :type boolean
    :initform nil
    :accessor toggle
    :property-p t)))

(defmethod fixed-update ((script pace-script) dt)
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body))

    (let* ((target (if (toggle script) (p2 script) (p1 script)))
           (offset (vec- target (location transform))))
      (when (<= (vec-mag offset) 10)
        (setf (toggle script) (not (toggle script)))
        (setf target (if (toggle script) (p2 script) (p1 script)))
        (setf offset (vec- target (location transform))))
      ;;Handle movement input
      (let ((movement-dir
             (vec-norm offset)))
        (setf (velocity bod) (vec* movement-dir (movement-speed script)))
        (unless (zerop (vec-mag (velocity bod)))
          (setf (rotation transform)
                (atan (vec-z (vec-norm (velocity bod))) (vec-x (vec-norm (velocity bod))))))))))

(defmethod update ((script pace-script))
  (ecs:with-components (script-entity script)
      ((transform transform)
       (bod rigid-body)
       (sprite sprite))

    (let* ((x (cos (rotation transform)))
           (y (sin (rotation transform)))
           (direction
            (if (>= (abs x) (abs y))
                (if (plusp x) :right :left)
                (if (plusp y) :down :up))))

      (setf (current-animation sprite)
            (ecase direction
              (:left :move-left)
              (:right :move-right)
              (:up :move-up)
              (:down :move-down))))))

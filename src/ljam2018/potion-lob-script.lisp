(in-package :ljam2018)

(define-component potion-lob-script (script)
  ((target :type math:vec3
           :initarg :target
           :accessor target
           :initform (vec3))
   (thunk  :type thunk
           :initarg :thunk
           :accessor thunk
           :initform (lambda (_ __)
                       (declare (ignore _ __))
                       nil))))

(defmethod fixed-update ((script potion-lob-script) dt)
  (ecs:with-components (script-entity script)
      ((transform transform))
    ;; idk some margin, i'm using 10 here
    (when (< (vec-dist (location transform) (target script)) 10.0)
      (funcall (thunk script) (location transform))
      (destroy-entity (script-entity script)))))

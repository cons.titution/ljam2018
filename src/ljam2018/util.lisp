(in-package #:ljam2018)

(defparameter +shade-1+ (media:color :r 85 :g 43 :b 114))
(defparameter +shade-2+ (media:color :r 149 :g 117 :b 171))
(defparameter +shade-3+ (media:color :r 115 :g 76 :b 142))
(defparameter +shade-4+ (media:color :r 58 :g 19 :b 85))
(defparameter +shade-5+ (media:color :r 35 :g 3 :b 57))

(defparameter +game-font-size+ 24)
(defparameter +game-font-color+ media.colors:*light-green*)

;;Multiplier applied in order to translate from some time amount into milliseconds
(defparameter +internal-time/ms+ (/ internal-time-units-per-second 1000))
(defparameter +internal-time/second+ internal-time-units-per-second)

(defparameter +ms/internal-time+ (/ 1000 internal-time-units-per-second))
(defparameter +seconds/internal-time+ (/ 1 internal-time-units-per-second))

(declaim (inline internal-time->seconds))
(defun internal-time->seconds (time)
  (* time +seconds/internal-time+))

(declaim (inline internal-time->ms))
(defun internal-time->ms (time)
  (* time +ms/internal-time+))

(declaim (inline seconds->internal-time))
(defun seconds->internal-time (seconds)
  (* seconds +internal-time/second+))

(declaim (inline ms->internal-time))
(defun ms->internal-time (ms)
  (* ms +internal-time/ms+))

(defun %call-after (time-seconds fn)
  (let ((timer (make-instance
                'dispatcher:dispatcher-timer
                :interval (timespan-from-seconds time-seconds))))
    (event-once
     (dispatcher:e_tick timer)
     nil
     (lambda (args)
       (declare (ignore args))
       (dispatcher:timer-stop timer)
       (funcall fn)))
    (dispatcher:timer-start timer)))

(defmacro %do-after (time-seconds &body body)
  `(%call-after ,time-seconds (lambda () ,@body)))

(defun %entity-live-p (entity)
  (slot-boundp entity 'entity-manager))

(defun %component-live-p (component)
  (slot-boundp component 'component-entity))

(in-package #:ljam2018)

;;Controls the most dangerous game of 1972

(define-component deathpong-controller (script)
  ((ui
    :type ui:grid
    :accessor ui
    :property-p nil)
   (score-ui
    :type ui:label
    :accessor score-ui)
   (time-ui
    :type ui:label
    :accessor time-ui)
   (status-ui
    :type ui:label
    :accessor status-ui)
   (potion-slots-ui
    :type ui:stack-panel
    :accessor potion-slots-ui
    :property-p nil)
   (score
    :type integer
    :initform 0
    :accessor score)
   (current-time
    :type real
    :initform 0.0
    :accessor current-time)
   (current-frame-number
    :type integer
    :initform 0
    :accessor current-frame-number)
   (map-width
    :type integer
    :reader map-width)
   (map-height
    :type integer
    :reader map-height)
   (potions-on-field
    :type list
    :initform nil
    :accessor potions-on-field)
   (potion-queued
    :type boolean
    :initform nil
    :accessor potion-queued)
   (camera
    :type camera
    :initform (make-instance 'camera :activep t)
    :reader camera
    :property-p t)
   (player
    :type deathpong-player
    :initform (make-instance 'deathpong-player :movement-speed 85)
    :reader player)
   (player-input-handle
    :type control:player-handle
    :initform (make-instance 'control:player-handle
                             :devices nil
                             :global t)
    :reader player-input-handle
    :property-p t)
   (%fps-calc
    :type fps-calculator
    :initform (make-instance 'fps-calculator)
    :reader %fps-calc)))

(defun %make-potion-slot-entry (potion)
  (make-instance
   'ui:content-control
   :width 48
   :height 48
   :data-context potion
   :content (make-instance 'ui:image :source (pickup-image potion))
   :background media.colors:*dark-green*
   :border-thickness 4))

(defparameter +controller-input-states+
  '(;;Exit
    :exit))

(defparameter +controller-input-maps+
  '((:keyboard/mouse
     (:scancode-escape :exit)
     :controller
     (:back :exit))))

(defparameter +player-input-states+
  '(;;Movement Buttons
    :left :right :up :down
    ;;Movement Axes
    :left/right :up/down

    ;;Action Buttons
    :dash :backstep :roll :run
    :drink :lob))

(defparameter +player-input-maps+
  '((:keyboard/mouse
     (:scancode-a :left
      :scancode-left :left

      :scancode-d :right
      :scancode-right :right

      :scancode-w :up
      :scancode-up :up

      :scancode-s :down
      :scancode-down :down

      :scancode-lshift :run
      :scancode-rshift :run
      :scancode-lalt :dash
      :scancode-space :roll
      :scancode-space :backstep
      :scancode-q :drink
      :scancode-e :lob)
     :controller
     (:dpad-left :left
      :dpad-right :right
      :dpad-up :up
      :dpad-down :down

      ;;:left/right & :up/down are axii
      :left-x :left/right
      :left-y :up/down

      :b :run
      :a :dash
      :a :backstep
      :b :backstep
      :b :roll
      :x :drink
      :right-shoulder :lob))))

(defun %make-game-label (&optional (text ""))
  (make-instance
   'ui:label
   :text text
   :width 80
   :font-size +game-font-size+
   :foreground +game-font-color+))

(defmethod initialize-instance :after ((controller deathpong-controller)
                                       &key
                                         (ui-control nil))
  (let ((ui (make-instance 'ui:grid :background media.colors:*transparent*))
        (score-label-ui (%make-game-label "Score:"))
        (score-ui (%make-game-label))
        (time-label-ui (%make-game-label "Time:"))
        (time-ui (%make-game-label))
        (status-ui (%make-game-label))
        (bottom-panel (make-instance 'ui:stack-panel :horizontal-alignment :center :vertical-alignment :bottom :margin '(0 0 2 20)))
        (potion-slots-ui (make-instance 'ui:stack-panel :horizontal-alignment :left :vertical-alignment :bottom :margin 5)))
    (ui:add-child bottom-panel score-label-ui)
    (ui:add-child bottom-panel score-ui)
    (ui:add-child bottom-panel time-label-ui)
    (ui:add-child bottom-panel time-ui)

    ;;A status label right in the middle of the screen
    (ui:add-child ui status-ui)

    (ui:add-child ui bottom-panel)
    (ui:add-child ui potion-slots-ui)
    (setf (ui controller) ui
          (score-ui controller) score-ui
          (time-ui controller) time-ui
          (status-ui controller) status-ui
          (potion-slots-ui controller) potion-slots-ui)

    (when ui-control
      (setf (ui:content ui-control) ui))))

(defun %generate-circle-vertices (radius num-samples)
  (loop :with delta := (/ (* 2 pi) num-samples)
        :repeat num-samples
        :for angle := delta :then (+ angle delta)
        :for x := (* radius (cos angle))
        :for y := (* radius (sin angle))
        :collecting (vec3 :x x :z y)))

(defun %add-components (entity &rest components)
  (mapc (lambda (c) (ecs:add-component entity c)) components))

(defun %on-ball-scored (controller ball)
  (incf (score controller) 10)

  (with-components (script-entity ball)
      ((ball-transform transform)
       (ball-rigid-body rigid-body))
    (setf (location ball-transform) (vec3 :x (/ (map-width controller) 2)
                                          :z (/ (map-height controller) 2))
          (velocity ball-rigid-body) (vec* (vec-norm (vec3 :x (random 1.0) :z (random 1.0)))
                                           420))))

(defun %spawn-potion-pickup (controller type
                             &key
                               (x (+ 40 (random (- (map-width controller) 80))))
                               (y (+ 20 (random (- (map-height controller) 40))))
                             &aux
                               (ecs:*entity-manager* (ecs:entity-manager controller)))
  (let ((pot (create-entity :name "pot")))
    ;; a sample potion
    (%add-components
     pot
     (make-instance 'sprite :path (pickup-image type))
     (make-instance 'transform :location (vec3 :x x :z y))
     (make-instance 'rigid-body)
     (make-instance 'rect-collider :width 35 :height 45)
     (make-instance 'pickup :payload :wall))
    (push pot (potions-on-field controller))))

(defmethod component-init ((controller deathpong-controller) entity)
  (call-next-method)

  ;;Add input mappings to ourselves
  (%add-components
   (script-entity controller)
   (make-instance 'player-input
                  :player-handle (player-input-handle controller)
                  :input-maps +controller-input-maps+)
   (make-instance 'input-states :states +controller-input-states+))

  (let* ((camera (create-entity :name "camera"))
         (player (create-entity :name "player"))
         (left-paddle (create-entity :name "left-paddle" :attributes '(:paddle t)))
         (right-paddle (create-entity :name "right-paddle" :attributes '(:paddle t)))
         (ball (create-entity :name "ball"))
         (map (create-entity :name "tiled-map"))
         (top-wall (create-entity :name "top-wall"))
         (bottom-wall (create-entity :name "bottom-wall"))
         (left-wall (create-entity :name "left-wall"))
         (right-wall (create-entity :name "right-wall"))
         (tiled-map (make-instance 'tiled-map :file-path "data/tiled/pong.tmx"))
         (map-width (map-width tiled-map))
         (map-height (map-height tiled-map)))
    ;;TODO hacky
    (setf (slot-value controller 'map-width) map-width
          (slot-value controller 'map-height) map-height)
    
    ;;Make a separate camera from the player in case we want to move it around
    (%add-components
     camera
     (make-instance 'transform)
     (camera controller))

    ;;Put walls to block off the play area

    ;;The player character
    (let* ((player-path "data/images/pong/leatherface.png")
           (player-image (make-instance 'media:image :path player-path)))
      (%add-components
       player
       (make-instance 'sprite :path player-path)
       (make-instance 'transform :location (vec3 :x 100 :z 100))
       (make-instance 'player-input
                      :player-handle (player-input-handle controller)
                      :input-maps +player-input-maps+)
       (make-instance 'rect-collider :width (media:image-width player-image)
                                     :height (media:image-height player-image))
       (make-instance 'rigid-body)
       (make-instance 'input-states :states +player-input-states+)
       (player controller)))

    ;;The tiled map
    (%add-components
     map
     (make-instance 'transform)
     tiled-map)

    (%add-components
     top-wall
     (make-instance 'rigid-body :kinematic-p t)
     (make-instance 'rect-collider :width map-width :height 1)
     (make-instance 'transform :location (vec3 :x (/ map-width 2))))
    (%add-components
     bottom-wall
     (make-instance 'rigid-body :kinematic-p t)
     (make-instance 'rect-collider :width map-width :height 1)
     (make-instance 'transform :location (vec3 :x (/ map-width 2) :z map-height)))

    (%add-components
     left-wall
     (make-instance 'rigid-body :kinematic-p t)
     (make-instance 'rect-collider :width 1 :height map-height)
     (make-instance 'transform :location (vec3 :x 0 :z (/ map-height 2))))
    (%add-components
     right-wall
     (make-instance 'rigid-body :kinematic-p t)
     (make-instance 'rect-collider :width 1 :height map-height)
     (make-instance 'transform :location (vec3 :x map-width :z (/ map-height 2))))

    ;; the ball!
    (%add-components
     ball
     (make-instance 'sprite :path "data/images/pong/ball.png")
     (make-instance 'transform :location (vec3 :x (/ map-width 2) :z (/ map-height 2)))
     (make-instance 'rigid-body :velocity (vec* (vec-norm (vec3 :x (random 1.0) :z (random 1.0)))
                                                220))
     (make-instance 'ball-script :map-width map-width :map-height map-height :ball-score-callback (lambda (ball) (%on-ball-scored controller ball)))
     (make-instance 'poly-collider
                    :vertices (%generate-circle-vertices 9 15)
                    :trigger-p t))

    ;;hopefully 2 paddles
    (let* ((paddle-path "data/images/pong/paddle.png")
           (paddle-image (make-instance 'media:image :path paddle-path))
           (paddle-width (media:image-width paddle-image))
           (paddle-height (media:image-height paddle-image)))
      ;;left
      (%add-components
       left-paddle
       (make-instance 'sprite :path paddle-path)
       (make-instance 'transform :location (vec3 :x (+ (/ paddle-width 2) 0)
                                                 :z (/ map-height 2)))
       (make-instance 'rigid-body)
       (make-instance 'paddle-script :balls (vector ball))
       (make-instance 'rect-collider :width paddle-width :height paddle-height))
      ;;and right
      (%add-components
       right-paddle
       (make-instance 'sprite :path paddle-path)
       (make-instance 'transform :location  (vec3 :x (- map-width  (/ paddle-width 2))
                                                  :z (/ map-height 2)))
       (make-instance 'rigid-body)
       (make-instance 'paddle-script :balls (vector ball))
       (make-instance 'rect-collider :width paddle-width :height paddle-height)))))

(defmethod fixed-update ((controller deathpong-controller) dt)
  (incf (current-time controller) dt)
  (dtext (-110 -70) "Time: ~5,2,,,'0F" (float (current-time controller)))
  (fps-time-tick (%fps-calc controller) dt)
  (%on-debug-time-tick dt))

(defmethod update ((controller deathpong-controller))
  (fps-frame-tick (%fps-calc controller))
  (dtext (-110 -50) "Frame: ~A" (current-frame-number controller))
  (dtext (-110 -30) "FPS: ~5,2,,,'0F" (float (fps (%fps-calc controller))))
  (with-components (script-entity controller)
      ((states input-states))
    (let ((exit (get-input states :exit)))
      (when (input-was-pressed exit)
        ;;TODO HACK you shouldn't assume that the app is ljam
        (%leave-gameplay (current-app))
        (%enter-main-menu (current-app)))))
  ;;See if it's time to spin up another potion
  (setf (potions-on-field controller) (delete-if (complement #'%entity-live-p) (potions-on-field controller)))

  (when (and (not (potion-queued controller))
             (< (length (potions-on-field controller)) 2))
    (setf (potion-queued controller) t)
    (%do-after 5
      (setf (potion-queued controller) nil)
      (%spawn-potion-pickup controller :wall)))

  (let* ((inventory (inventory (player controller)))
         (ui-panel (potion-slots-ui controller)))
    ;;Synchronize the UI's list
    ;;Super hacky hack because I don't want to figure out how to differentiate
    ;;between the two potions in the inventory atm and I'm out of time and brain
    (loop :while (> (length inventory) (length (ui:children ui-panel)))
          :do (ui:add-child ui-panel (%make-potion-slot-entry (first inventory))))
    (loop :while (< (length inventory) (length (ui:children ui-panel)))
          :do (ui:remove-child ui-panel (first (ui:children ui-panel))))

    ;; (let ((removals nil)
    ;;       (additions inventory))
    ;;   (dolist (entry (ui:children ui-panel))
    ;;     (if (member (ui:data-context entry) inventory)
    ;;         (setf additions (delete (ui:data-context entry) additions))
    ;;         (push entry removals)))
    ;;   (dolist (removal removals)
    ;;     (ui:remove-child ui-panel removal))
    ;;   (dolist (addition additions)
    ;;     (ui:add-child ui-panel (%make-potion-slot-entry addition))))
    )
  ;;Update the labels
  (setf (ui:text (score-ui controller)) (format nil "~4,'0D " (score controller))
        (ui:text (time-ui controller)) (format nil "~3,'0D" (truncate (current-time controller)))))

(defmethod component-uninit ((controller deathpong-controller) entity)
  (call-next-method))

(in-package #:ljam2018)

(defgeneric pickup-image (item))

(define-component pickup (script)
  ((payload
    :initarg :payload
    :reader payload
    :property-p t)))

(defmethod on-collision ((script pickup) col)
  (let ((other-entity (component-entity (mar:collision-event-collider col))))
    (dtext (-50 200 :time 2) "Pickup!")
    (send-event (entity-events other-entity) 'on-pickup script)))

(in-package #:mar)

(defclass game-control (ui:component)
  ((%game
    :type game
    :reader game))
  (:default-initargs
   :background media.colors:*black*
   :horizontal-content-alignment :stretch
   :vertical-content-alignment :stretch)
  (:documentation
   "A `ui:component' that initializes a game when loaded and uninitializes it when unloaded."))

(defmethod initialize-instance :after ((game-control game-control)
                                       &key
                                         (initial-scene (make-instance 'scene)))
  (setf (slot-value game-control '%game) (make-instance 'game :initial-scene initial-scene)))

;;TODO hack
(defmethod ui::load-component ((game-control game-control))
  (call-next-method)

  (let ((dispatcher (dispatcher:dispatcher game-control)))
    (when (typep dispatcher 'sol.sdl2-driver:sdl2-dispatcher)
      ;;TODO workaround until better dispatcher event handling mechanism is in place
      (setf (sol.sdl2-driver:wait-behaviour dispatcher) :poll))

    (event-subscribe
     (dispatcher:e_dispatcher-inactive dispatcher)
     game-control
     '%game-control-on-idle))

  (game-init (game game-control))

  (event-subscribe
   (e_frame-tick (game game-control))
   game-control
   '%game-control-on-frame-tick))

(defmethod ui:draw ((game-control game-control) renderer)
  (game-render (game game-control) renderer))

;;TODO hack
(defmethod ui::unload-component ((game-control game-control))
  (event-unsubscribe
   (e_frame-tick (game game-control))
   game-control)

  (game-uninit (game game-control))

  (let ((dispatcher (dispatcher:dispatcher game-control)))
    (event-unsubscribe
     (dispatcher:e_dispatcher-inactive dispatcher)
     game-control)

    (when (typep dispatcher 'sol.sdl2-driver:sdl2-dispatcher)
      ;;TODO workaround until better dispatcher event handling mechanism is in place
      (setf (sol.sdl2-driver:wait-behaviour dispatcher) :wait)))

  (call-next-method))

(defun %game-control-on-idle (game-control args)
  (declare (ignore args))
  (game-update (game game-control)))

(defun %game-control-on-frame-tick (game-control args)
  (declare (ignore args))
  (ui:invalidate-arrange game-control))

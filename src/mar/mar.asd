(defsystem #:mar
  :name "mar"
  :version "0.0.0"
  :description "Base game runtime."
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "zlib/libpng License <http://opensource.org/licenses/zlib-license.php>"
  :serial t
  :components
  ((:file "package")
   (:file "util")
   (:module "components"
    :serial t
    :components
    ((:file "package")
     (:file "input-states")
     (:file "transform")
     (:file "camera")
     (:file "text")
     (:file "tiled-map")
     (:file "physics")
     (:file "player-input")
     (:file "script")
     (:file "sprite")))
   (:file "input-system")
   (:file "physics-system")
   (:file "render-system")
   (:file "scene")
   (:file "game-loop")
   (:file "game")
   (:file "game-control"))
  :depends-on
  (#:alexandria
   #:defpackage-plus
   #:cl-tiled
   #:luna
   #:sol
   #:sol.sdl2-driver
   #:raw-bindings-sdl2))

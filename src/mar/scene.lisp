(in-package #:mar)

(defclass scene (serial:serializable)
  ((%name
    :type string
    :initarg :name
    :initform ""
    :reader name)
   (%entities.components
    :type list
    :initform nil
    :initarg :entities.components
    :accessor %entities.components)))

(defmethod serial:serialized-slots ((scene scene))
  '(%name %entities.components))

;; (defun load-scene (name
;;                    &aux
;;                      (filename (%make-scene-filename name)))
;;   (serial:deserialize-file filename))

;; (defun save-scene (scene
;;                    &aux
;;                      (filename (%make-scene-filename (name scene))))
;;   (serial:serialize-file scene filename)
;;   (values))

(defun %scene-init (scene)
  (loop :for (entity . components) :in (%entities.components scene)
     :do
     (ecs:add-entity entity)
     (dolist (c components)
       (ecs:add-component entity c))))

;; (defun scene-snapshot (scene)
;;   (setf (entities.components scene) nil)
;;   (dolist (entity (ecs:entities (entity-manager scene)))
;;     (push (cons entity (ecs:list-components entity))
;;           (entities.components scene)))
;;   (setf (entities.components scene) (nreverse (entities.components scene))))

(defun %scene-uninit (scene)
  (dolist (entity (map 'list #'identity (ecs:entities ecs:*entity-manager*)))
    (ecs:destroy-entity entity)))

;; (defun %make-scene-filename (scene-name)
;;   (make-pathname
;;    :directory (list :relative "data/scenes")
;;    :name scene-name
;;    :type "lsc"))

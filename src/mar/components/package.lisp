;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:defpackage+-user-1)

(defpackage+ #:mar.components
  (:use
   #:alexandria
   #:cl)
  (:inherit
   #:sol
   #:mar
   #:luna.ecs)
  (:local-nicknames
   (#:media #:sol.media)
   (#:media.colors #:sol.media.colors)
   (#:input #:sol.input)
   (#:control #:luna.control)
   (#:math #:luna.math)
   (#:serial #:luna.serial)
   (#:shapes #:luna.shapes))
  (:export
   #:transform
   #:location
   #:rotation
   #:scale

   #:sprite
   #:animations
   #:current-animation

   #:player-input
   #:player-handle
   #:input-map-chain

   #:input-state
   #:input-state
   #:input-name
   #:input-is-held
   #:input-was-pressed
   #:input-time-pressed
   #:input-was-released
   #:input-time-released
   #:input-value
   #:input-time-since-pressed
   #:input-time-since-released

   #:input-states
   #:get-input

   #:camera
   #:activep

   #:text

   #:tiled-map
   #:file-path
   #:map-width
   #:map-height
   #:background-color

   #:collider
   #:poly
   #:trigger-p
   #:bounding-sphere-radius

   #:rect-collider
   #:width
   #:height

   #:poly-collider

   #:rigid-body
   #:mass
   #:kinematic-p
   #:velocity

   #:script
   #:script-entity-manager
   #:script-event-manager
   #:script-entity

   #:fixed-update
   #:update
   #:on-collision
   #:on-trigger))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar.components)

(defclass input-state (serial:serializable)
  ((input-name
    :type keyword
    :initarg :name
    :initform (error "input-state: must supply input name")
    :reader input-name)
   (input-is-held
    :type :boolean
    :initarg :is-held
    :initform nil
    :accessor input-is-held)
   (input-was-pressed
    :type :boolean
    :initarg :was-pressed
    :initform nil
    :accessor input-was-pressed)
   (input-time-pressed
    :type real
    :initarg :time-pressed
    :initform 0
    :accessor input-time-pressed)
   (input-time-since-pressed
    :type real
    :initform 0
    :accessor input-time-since-pressed)
   (input-was-released
    :type :boolean
    :initarg :was-released
    :initform nil
    :accessor input-was-released)
   (input-time-released
    :type real
    :initarg :time-released
    :initform 0
    :accessor input-time-released)
   (input-time-since-released
    :type real
    :initform 0
    :accessor input-time-since-released)
   (input-value
    :type :number
    :initarg :value
    :initform 0
    :accessor input-value)))

(defmethod serial:serialized-slots ((state input-state))
  '(input-name))

(define-component input-states ()
  ((input-states
    :type list
    :reader input-states
    :property-p t)))

(defmethod initialize-instance :after ((input-states input-states) &key (states nil) &allow-other-keys)
  (setf (slot-value input-states 'input-states)
        (mapcar (lambda (n) (cons n (make-instance 'input-state :name n)))
                states)))

(defun get-input (input-states state)
  (assoc-value (input-states input-states) state))

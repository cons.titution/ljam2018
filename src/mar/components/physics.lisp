;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar.components)

(define-component collider ()
  ((trigger-p
    :type boolean
    :initarg :trigger-p
    :initform nil
    :accessor trigger-p
    :property-p t)
   (bounding-sphere-radius
    :type real
    :initform 0
    :accessor bounding-sphere-radius)))

(define-component poly-collider (collider)
  ((poly
    :type poly
    :reader poly
    :property-p t)))

(defmethod initialize-instance :after ((collider poly-collider)
                                       &key
                                         (vertices ())
                                         &allow-other-keys)
  (setf (slot-value collider 'poly)
        (make-instance
         'shapes:poly
         :vertices vertices))

  (setf (bounding-sphere-radius collider)
        (loop
          :for vertex :across (shapes:poly-vertices (poly collider))
          :maximize (math:vec-mag vertex))))

(defun vertices (poly-collider)
  (shapes:poly-vertices (poly poly-collider)))

(defun edges (poly-collider)
  (shapes:poly-edges (poly poly-collider)))

(define-component rect-collider (poly-collider)
  ((width
    :type number
    :initarg :width
    :reader width
    :property-p t)
   (height
    :type number
    :initarg :height
    :reader height
    :property-p t))
  (:default-initargs
   :width 1 :height 1))

(defmethod initialize-instance :after ((collider rect-collider) &key &allow-other-keys)
  (with-slots (poly width height bounding-sphere-radius) collider
    (setf poly
          (make-instance
           'shapes:poly
           :vertices
           (list
            (math:vec3 :x (- (/ width 2)) :z (- (/ height 2)))
            (math:vec3 :x (+ (/ width 2)) :z (- (/ height 2)))
            (math:vec3 :x (+ (/ width 2)) :z (+ (/ height 2)))
            (math:vec3 :x (- (/ width 2)) :z (+ (/ height 2))))))
    (setf (bounding-sphere-radius collider)
        (loop
          :for vertex :across (shapes:poly-vertices poly)
          :maximize (math:vec-mag vertex)))))

(define-component rigid-body ()
  ((mass
    :type real
    :initarg :mass
    :initform 1
    :accessor mass
    :property-p t)
   (kinematic-p
    :type boolean
    :initarg :kinematic-p
    :initform nil
    :accessor kinematic-p
    :property-p t)
   (velocity
    :type vec3
    :initarg :velocity
    :initform (math:vec3)
    :accessor velocity
    :property-p t)))

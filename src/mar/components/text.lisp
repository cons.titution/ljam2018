;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar.components)

(define-component text ()
  ((text
    :type media:text
    :reader text-text
    :property-p t)
   (offset-x
    :type real
    :initarg :offset-x
    :initform 0
    :reader text-offset-x
    :property-p t)
   (offset-y
    :type real
    :initarg :offset-y
    :initform 0
    :reader text-offset-y
    :property-p t)))

(defmethod initialize-instance :after ((comp text)
                                       &key
                                         (string "")
                                         (font (make-instance 'media:font))
                                         (color media.colors:*black*))
  (setf (slot-value comp 'text)
        (make-instance
         'media:text
         :string string
         :font font
         :color color)))

(defmethod component-init ((comp text) entity)
  (subscribe-event
   (global-events entity)
   :on-render
   comp
   '%text-on-render))

(defmethod component-uninit ((comp text) entity)
  (unsubscribe-event
   (global-events entity)
   :on-render
   comp))

(defun %text-on-render (comp renderer)
  (when-let ((transform (get-component (component-entity comp) 'transform)))
    (media:render-draw-text
     renderer
     (text-text comp)
     (+ (text-offset-x comp) (math:vec-x (location transform)))
     (+ (text-offset-y comp) (math:vec-z (location transform))))))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar.components)

(define-component script ()
  ())

(defun script-entity (script)
  (component-entity script))

(defun script-entity-manager (script)
  (entity-manager script))

(defun script-event-manager (script)
  (global-events script))

(defgeneric fixed-update (script dt)
  (:documentation "Update `script' by `dt' seconds. Might be called multiple times per frame.")
  (:method (script dt)
    (declare (ignore script dt))))

(defgeneric update (script)
  (:documentation "Update `script' once per frame.")
  (:method (script)
    (declare (ignore script))))

(defgeneric on-collision (script col)
  (:method (script col)
    (declare (ignore script col))))

(defgeneric on-trigger (script col)
  (:method (script col)
    (declare (ignore script col))))

(defmethod component-init :around ((script script) entity)
  (unwind-protect
       (call-next-method)
    (subscribe-event
     (script-event-manager script)
     :time-tick
     script
     'fixed-update)
    (subscribe-event
     (script-event-manager script)
     :frame-tick
     script
     '%script-on-frame-tick)
    (subscribe-event
     (entity-events entity)
     :on-collision
     script
     'on-collision)
    (subscribe-event
     (entity-events entity)
     :on-trigger
     script
     'on-trigger)))

(defmethod component-uninit :around ((script script) entity)
  (unsubscribe-event
   (entity-events entity)
   :on-trigger
   script)
  (unsubscribe-event
   (entity-events entity)
   :on-collision
   script)
  (unsubscribe-event
   (script-event-manager script)
   :time-tick
   script)
  (unsubscribe-event
   (script-event-manager script)
   :frame-tick
   script)
  (unsubscribe-event
   (script-event-manager script)
   :time-tick
   script)
  (call-next-method))

(defun %script-on-frame-tick (script args)
  (declare (ignore args))
  (update script))

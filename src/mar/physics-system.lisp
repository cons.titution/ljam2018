;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar)

(defparameter +min-td+ -0.0001)
(declaim (type single-float +min-td+))

(defparameter +time-padding+ 0.0001)
(declaim (type single-float +time-padding+))

(defparameter *show-wireframes* nil)

(defstruct physics-entry
  (entity nil :type ecs:entity)
  (transform nil :type components:transform)
  (collider nil :type components:collider)
  (rigid-body nil :type (or null components:rigid-body)))

(defstruct collision-entry
  (td 0.0 :type single-float)
  (mtd-dir math:+vec-zero+ :type math:vec3)
  (event-type :collision :type (member :collision :trigger))
  (e2 nil :type physics-entry))

(defstruct collision-event
  (td 0.0 :type single-float)
  (mtd-dir math:+vec-zero+ :type math:vec3)
  (collider nil :type components:collider))

(defclass physics-system (ecs:system)
  ((entries
    :type (vector physics-entry)
    :initform (make-array '(512)
                          :element-type 'physics-entry
                          :adjustable t :fill-pointer 0
                          :initial-element (make-physics-entry
                                            :entity (make-instance 'ecs:entity)
                                            :transform (make-instance 'components:transform)
                                            :collider (make-instance 'components:collider)))
    :accessor physics-system-entries)
   (pending-col
    :type (vector collision-entry)
    :initform (make-array 16
                          :element-type 'collision-entry
                          :fill-pointer 0
                          :initial-element
                          (make-collision-entry
                           :e2
                           (make-physics-entry
                            :entity (make-instance 'ecs:entity)
                            :transform (make-instance 'components:transform)
                            :collider (make-instance 'components:collider))))
    :reader pending-col)
   (collision-mtd-vecs
    :initform nil
    :accessor collision-mtd-vecs)
   (overlap-mtd-vecs
    :initform nil
    :accessor overlap-mtd-vecs)))

(defmethod ecs:system-configure ((system physics-system))
  (ecs:subscribe-event
   (ecs:event-manager system)
   :time-tick
   system
   '%physics-system-on-time-tick)

  (ecs:subscribe-event
   (ecs:event-manager system)
   :on-render
   system
   '%physics-system-on-render :priority 10000)

  (ecs:subscribe-event
   (ecs:event-manager system)
   :components-changed
   system
   '%physics-system-on-components-changed))

(defmethod ecs:system-unconfigure ((system physics-system))
  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :components-changed
   system)

  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :on-render
   system)

  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :time-tick
   system))

(defun %physics-system-on-time-tick (system dt
                                     &aux (pending-col (pending-col system)))
  (declare (optimize (speed 3)))
  (declare (physics-system system)
           (type single-float dt)
           (type (vector collision-entry) pending-col))
  (setf (collision-mtd-vecs system) nil)
  (setf (overlap-mtd-vecs system) nil)

  (loop
    :with entries := (the (vector physics-entry) (physics-system-entries system))
    :with elen := (length entries)
    :for i :of-type fixnum :from 0 :below elen
    :for e1 := (aref entries i)
    :for c1 := (physics-entry-collider e1)
    :for t1 := (physics-entry-transform e1)
    :for b1 := (physics-entry-rigid-body e1)
    :for c1-rad :of-type single-float := (the single-float (components:bounding-sphere-radius c1))
    :for b1-vel :of-type single-float := (if b1 (math:vec-sqr-mag (components:velocity b1)) 0.0)
    :for checked-entries := (list e1)
    :for effective-dt :of-type single-float := dt
    :do
       (flet ((recheck-collisions ()
                (setf (fill-pointer pending-col) 0)
                (loop
                  :for j :from (1+ i) :below elen
                  :for e2 := (aref entries j)
                  :for c2 := (physics-entry-collider e2)
                  :for t2 := (physics-entry-transform e2)
                  :for b2 := (physics-entry-rigid-body e2)
                  :for max-dist := (+ c1-rad
                                      (the single-float (components:bounding-sphere-radius c2))
                                      (* b1-vel effective-dt))
                  :unless
                  (or
                   ;;They are the same entry
                   (find e2 checked-entries)
                   ;;They are both kinematic
                   (and b1 (components:kinematic-p b1)
                        b2 (components:kinematic-p b2))
                   ;;They are too far appart
                   (> (math:vec-dist (components:location t1) (components:location t2)) max-dist))
                  :do
                     (when-let ((col (%test-colliders effective-dt e1 e2 (math:vec- (components:location t1) (components:location t2)))))
                       (if (and (minusp (collision-entry-td col))
                                (eq (collision-entry-event-type col) :collision))
                           ;;Overlap collision.
                           ;;Put it at the front
                           ;;and return immediately
                           (progn
                             (setf (fill-pointer pending-col) 1)
                             (setf (aref pending-col 0) col)
                             (return))
                           (vector-push col pending-col)))
                  :finally
                     ;;Sort by collision time, with overlaps happening first
                     (setf pending-col (sort pending-col #'< :key #'collision-entry-td)))))
         ;;From the possible collisions, resolve by collision time
         ;;Overlapping collisions are time 0
         ;;event-type is either :trigger, :collision, or nil depending on
         ;;whether it's a trigger or collision response
         (recheck-collisions)
         (loop
           :with j := 0
           :while (< j (length pending-col))
           :for entry := (aref pending-col j)
           :for td := (collision-entry-td entry)
           :for mtd-dir := (collision-entry-mtd-dir entry)
           :for event-type := (collision-entry-event-type entry)
           :for e2 := (collision-entry-e2 entry)
           ;;A full blown collision
           :do (push e2 checked-entries)
           :if (eq event-type :collision) :do
             (progn
               (cond
                 ((minusp td)          ;Overlap collision
                  ;;Adjust the location to fix the overlap
                  (setf (components:location t1) (math:vec- (components:location t1) (math:vec* mtd-dir (min td +min-td+))))
                  (push (cons (components:location t1) mtd-dir) (overlap-mtd-vecs system)))
                 (t ;;Future collision Note: Safe to use b1 here since we need a velocity for future collisions
                  ;;Don't travel the full col-time
                  (let ((col-time (max (- td +time-padding+) 0.0)))
                    ;;Travel for as long as we can before colliding
                    (unless (< col-time +time-padding+)
                      (setf (components:location t1)
                            (math:vec+ (components:location t1)
                                       (math:vec* (components:velocity b1) col-time)))

                      ;;Decrease the effective dt, since we moved partially
                      (decf effective-dt col-time)))

                  ;;Oppose the velocity
                  (setf (components:velocity b1)
                        (math:vec- (components:velocity b1)
                                   (math:vec-no-norm-project mtd-dir (components:velocity b1))))
                  (push (cons (components:location t1) mtd-dir) (collision-mtd-vecs system))))

               (ecs:send-event
                (ecs:entity-events (physics-entry-entity e1))
                :on-collision
                (make-collision-event :td td :mtd-dir mtd-dir :collider (physics-entry-collider e2)))
               (ecs:send-event
                (ecs:entity-events (physics-entry-entity e2))
                :on-collision
                (make-collision-event :td td :mtd-dir mtd-dir :collider (physics-entry-collider e1)))

               ;;Since we modified our location and velocity
               ;;we need to re-calculate our collisions
               (recheck-collisions)
               (setf j 0))

             ;;Just a trigger, signal events and carry on
           :else :if (eq event-type :trigger) :do
             (progn
               (when mtd-dir
                 (if (minusp td)
                     ;;Overlap trigger
                     (progn
                       (push (cons (components:location t1) (math:vec* mtd-dir (- td))) (overlap-mtd-vecs system))
                       (push (cons (components:location (physics-entry-transform e2)) (math:vec* mtd-dir td)) (overlap-mtd-vecs system)))
                     ;;Future trigger
                     (progn
                       (push (cons (components:location t1) mtd-dir) (collision-mtd-vecs system))
                       (push (cons (components:location (physics-entry-transform e2)) mtd-dir) (collision-mtd-vecs system)))))
               (ecs:send-event
                (ecs:entity-events (physics-entry-entity e1))
                :on-trigger
                (make-collision-event :td td :mtd-dir mtd-dir :collider (physics-entry-collider e2)))
               (ecs:send-event
                (ecs:entity-events (physics-entry-entity e2))
                :on-trigger
                (make-collision-event :td td :mtd-dir (math:vec- mtd-dir) :collider (physics-entry-collider e1)))

               (incf j))
           :finally
              ;;Now that we've resolved all possible collisions,
              ;;move according to velocity
              (when (and b1 (not (components:kinematic-p b1)))
                (setf (components:location t1)
                      (math:vec+ (components:location t1)
                                 (math:vec* (components:velocity b1) effective-dt)))))))

  (setf (collision-mtd-vecs system) (nreverse (collision-mtd-vecs system)))
  (setf (overlap-mtd-vecs system) (nreverse (overlap-mtd-vecs system))))

(defun %physics-system-on-render (system renderer)
  (unless *show-wireframes*
    (return-from %physics-system-on-render))
  (loop
    :with entries := (physics-system-entries system)
    :with elen := (length entries)
    :for i :from 0 :below elen
    :for e := (aref entries i)
    :for offset := (components:location (physics-entry-transform e))
    :for c := (physics-entry-collider e)
    :for p := (components:poly c)
    :do
       (media:render-draw-ellipse
        renderer
        (math:vec-x offset) (math:vec-z offset)
        (components:bounding-sphere-radius c) (components:bounding-sphere-radius c)
        :stroke media.colors:*light-blue*)
       (loop
         :for index :below (shapes:poly-num-vertices p)
         :for vertex := (shapes:poly-vertex p index)
         :for edge := (shapes:poly-edge p index)
         :for p1 := (math:vec+ vertex offset)
         :for p2 := (math:vec+ p1 edge)
         :do
            (media:render-draw-line
             renderer
             (math:vec-x p1) (math:vec-z p1)
             (math:vec-x p2) (math:vec-z p2)
             :color media.colors:*blue*))
       (loop
         :for index :below (shapes:poly-num-vertices p)
         :for vertex := (shapes:poly-vertex p index)
         :for px := (math:vec+ vertex offset)
         :do
            (media:render-draw-point renderer (math:vec-x px) (math:vec-z px) :color media.colors:*red*)))

  (dolist (cons (overlap-mtd-vecs system))
    (let* ((offset (car cons))
           (dir-and-mag (cdr cons))
           (dir (math:vec-norm dir-and-mag))
           (mag (math:vec-mag dir-and-mag)))
      (%render-arrow
       renderer
       offset dir (* 5 mag)
       media.colors:*red*)))

  (dolist (cons (collision-mtd-vecs system))
    (let* ((offset (car cons))
           (dir (cdr cons))
           (mag 25))
      (%render-arrow
       renderer
       offset dir mag
       media.colors:*lime-green*))))

(defun %physics-system-on-components-changed (system entity
                                              &aux
                                                (transform (ecs:get-component entity 'components:transform))
                                                (collider (or (ecs:get-component entity 'components:poly-collider)
                                                              (ecs:get-component entity 'components:rect-collider)))
                                                (rigid-body (ecs:get-component entity 'components:rigid-body))
                                                (entry (find entity (physics-system-entries system) :key #'physics-entry-entity)))
  (cond
    ((and entry (or (null transform) (null collider)))
     (setf (physics-system-entries system) (delete entry (physics-system-entries system))))
    (entry
     (setf (physics-entry-transform entry) transform
           (physics-entry-collider entry) collider
           (physics-entry-rigid-body entry) rigid-body))
    ((and transform collider)
     (vector-push-extend  (make-physics-entry
                           :entity entity
                           :transform transform
                           :collider collider
                           :rigid-body rigid-body)
                          (physics-system-entries system)))))

(defun %test-colliders (dt e1 e2 offset
                        &aux
                          (c1 (physics-entry-collider e1))
                          (c2 (physics-entry-collider e2))
                          (b1 (physics-entry-rigid-body e1)))
  (when-let ((event-type
              (cond
                ((or (components:trigger-p c1) (components:trigger-p c2))
                 :trigger)
                ((and b1 (not (components:kinematic-p b1)))
                 :collision)
                (t
                 nil))))
    (let ((velocity (if b1 (components:velocity b1) math:+vec-zero+)))
      (multiple-value-bind (col-mtd td)
          (physics:collide-vel-mtd
           (components:poly c1) (components:poly c2)
           offset velocity dt)
        (when col-mtd
          (make-collision-entry :td td :mtd-dir col-mtd :event-type event-type :e2 e2))))))

(defun %render-arrow (renderer p d len color
                      &aux
                        (draw-len len)
                        (arrow-len (/ draw-len 3))
                        (angle (atan (- (math:vec-z d)) (math:vec-x d)))
                        (arrow-angle (/ pi 8))
                        (p2 (math:vec+ p (math:vec* d draw-len)))
                        (p3 (math:vec3 :x (- (math:vec-x p2) (* (cos (+ angle arrow-angle)) arrow-len))
                                       :z (+ (math:vec-z p2) (* (sin (+ angle arrow-angle)) arrow-len))))
                        (p4 (math:vec3 :x (- (math:vec-x p2) (* (cos (- angle arrow-angle)) arrow-len))
                                       :z (+ (math:vec-z p2) (* (sin (- angle arrow-angle)) arrow-len)))))
  (media:render-draw-line
   renderer
   (round (math:vec-x p)) (round (math:vec-z p))
   (round (math:vec-x p2)) (round (math:vec-z p2))
   :color color)
  (media:render-draw-line
   renderer
   (math:vec-x p2) (math:vec-z p2)
   (math:vec-x p3) (math:vec-z p3)
   :color color)
  (media:render-draw-line
   renderer
   (math:vec-x p2) (math:vec-z p2)
   (math:vec-x p4) (math:vec-z p4)
   :color color))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar)

(defclass input-system (ecs:system)
  ;; Input states that were updated 'this time tick'
  ;; Used to reset their "was-pressed" and "was-released"
  ((input-manager
    :type input-manager
    :initform (error "input-system: must supply input-manager")
    :initarg :input-manager
    :reader input-manager)
   (current-time-seconds
    :type real
    :initform 0.0
    :accessor current-time-seconds)
   (updated-inputs
    :type list
    :initform nil
    :accessor updated-inputs)
   ;; Input states that were updated 'this time tick' by non-button mouse inputs (x, y, wheel)
   ;; Because these mouse events provide no corresponding "stop" event, we simulate one
   (mouse-inputs
    :type list
    :initform nil
    :accessor mouse-inputs)
   ;; Last known mouse x position
   (mouse-x
    :type integer
    :initform 0
    :accessor mouse-x)
   ;; Last known mouse y position
   (mouse-y
    :type integer
    :initform 0
    :accessor mouse-y)))

(defmethod ecs:system-configure ((system input-system)
                             &aux (input (input-manager system)))

  (ecs:subscribe-event
   (ecs:event-manager system)
   :time-tick
   system
   '%input-on-time-tick
   :priority 1000)

  (ecs:subscribe-event
   (ecs:event-manager system)
   :frame-tick
   system
   '%input-on-frame-tick
   :priority 1000)

  (event-subscribe
   (input:e_key input)
   system
   '%on-key)
  (event-subscribe
   (input:e_mouse-move input)
   system
   '%on-mouse)
  (event-subscribe
   (input:e_mouse-button input)
   system
   '%on-mouse-button)
  (event-subscribe
   (input:e_mouse-wheel input)
   system
   '%on-mouse-wheel)
  (event-subscribe
   (input:e_controller-button input)
   system
   '%on-controller-button)
  (event-subscribe
   (input:e_controller-axis input)
   system
   '%on-controller-axis))

(defmethod ecs:system-unconfigure ((system input-system)
                               &aux (input (input-manager system)))
  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :time-tick
   system)

  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :frame-tick
   system)

  (event-unsubscribe
   (input:e_key input)
   system)
  (event-unsubscribe
   (input:e_mouse-move input)
   system)
  (event-unsubscribe
   (input:e_mouse-button input)
   system)
  (event-unsubscribe
   (input:e_mouse-wheel input)
   system)
  (event-unsubscribe
   (input:e_controller-button input)
   system)
  (event-unsubscribe
   (input:e_controller-axis input)
   system))

(defparameter +%controller-axis-dead-zone+ 0.14)

(defun %input-on-time-tick (system dt)
  (incf (current-time-seconds system) dt)

  (ecs:do-components (entity-id (states components:input-states))
      (:entity-manager (ecs:entity-manager system))
    (dolist (input-pair (components:input-states states))
      (destructuring-bind (name . state)
          input-pair
        (declare (ignore name))
        (if (components:input-is-held state)
            (setf (components:input-time-since-pressed state)
                  (- (current-time-seconds system) (components:input-time-pressed state)))
            (setf (components:input-time-since-released state)
                  (- (current-time-seconds system) (components:input-time-released state))))))))

(defun %input-on-frame-tick (system args)
  (declare (ignore args))
  (dolist (input (updated-inputs system))
    (setf
     (components:input-was-pressed input) nil
     (components:input-was-released input) nil))
  (setf (updated-inputs system) ())

  (dolist (input (mouse-inputs system))
    (setf (components:input-is-held input) nil
          (components:input-value input) 0
          (components:input-was-pressed input) nil
          (components:input-was-released input) t
          (components:input-time-released input) (current-time-seconds system))
    (pushnew input (updated-inputs system)))
  (setf (mouse-inputs system) ()))

(defun %on-key (system args
                &aux
                  (dev (input:device args))
                  (scheme :keyboard/mouse)
                  (name (input:key args))
                  (heldp (input:key-pressed args))
                  (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-mouse (system args
                  &aux
                    (dev (input:device args))
                    (scheme :keyboard/mouse)
                    (name-x :mouse-x)
                    (heldp-x (/= (input:x args) (mouse-x system)))
                    (val-x (input:x args))
                    (name-y :mouse-y)
                    (heldp-y (/= (input:y args) (mouse-y system)))
                    (val-y (input:y args)))
  (%update-input system dev scheme name-x heldp-x val-x)
  (%update-input system dev scheme name-y heldp-y val-y)
  (setf (mouse-x system) val-x
        (mouse-y system) val-y))

(defun %on-mouse-button (system args
                         &aux
                           (dev (input:device args))
                           (scheme :keyboard/mouse)
                           (name (input:button args))
                           (heldp (input:button-pressed args))
                           (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-mouse-wheel (system args
                        &aux
                          (dev (input:device args))
                          (scheme :keyboard/mouse)
                          (name :mouse-wheel)
                          (heldp (/= (input:delta args) 0))
                          (val (input:delta args)))
  (%update-input system dev scheme name heldp val))

(defun %on-controller-axis (system args
                            &aux
                              (dev (input:device args))
                              (scheme :controller)
                              (name (input:axis args))
                              (val-deadzone-p
                               (<= (abs (input:value args))
                                   +%controller-axis-dead-zone+))
                              (heldp (not val-deadzone-p))
                              (val (if heldp (input:value args) 0)))
  (%update-input system dev scheme name heldp val))

(defun %on-controller-button (system args
                              &aux
                                (dev (input:device args))
                                (scheme :controller)
                                (name (input:button args))
                                (heldp (input:button-pressed args))
                                (val (if heldp 1 0)))
  (%update-input system dev scheme name heldp val))

(defun %update-input (system device scheme-type name heldp val)
  (ecs:do-components (entity-id
                      (input components:player-input)
                      (states components:input-states))
      (:entity-manager (ecs:entity-manager system))
    (when (%using-device input device)
      (dolist (input-state (%get-mapped-input-states
                            (components:input-map-chain input) states
                            scheme-type name))
        (%update-input-state system input-state heldp val)
        (if (member name '(:mouse-wheel :mouse-x :mouse-y))
            (pushnew input-state (mouse-inputs system))
            (pushnew input-state (updated-inputs system)))))))

(defun %update-input-state (system input heldp val)
  (let* ((was-pressed (and heldp (not (components:input-is-held input))))
         (time-pressed (or (and was-pressed (current-time-seconds system))
                           (components:input-time-pressed input)))
         (time-since-pressed (- (current-time-seconds system) time-pressed))
         (was-released (and (not heldp) (components:input-is-held input)))
         (time-released (or (and was-released (current-time-seconds system))
                            (components:input-time-released input)))
         (time-since-released (- (current-time-seconds system) time-released)))
    (setf (components:input-is-held input) heldp
          (components:input-value input) val
          (components:input-was-pressed input) was-pressed
          (components:input-time-pressed input) time-pressed
          (components:input-time-since-pressed input) time-since-pressed
          (components:input-was-released input) was-released
          (components:input-time-released input) time-released
          (components:input-time-since-released input) time-since-released)))

(defun %using-device (player-input device-id)
  (or (null (control:devices (components:player-handle player-input)))
      (member device-id (control:devices (components:player-handle player-input)))))

(defun %get-mapped-input-states (input-map-chain input-states scheme-type input-name)
  (loop
    :for mapped-name
      :in (input:chain-get-mapped-input-names input-map-chain scheme-type input-name)
    :if (components:get-input input-states mapped-name)
      :collect :it))

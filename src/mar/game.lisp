(in-package #:mar)

(defclass game ()
  ((%initial-scene
    :type scene
    :initarg :initial-scene
    :initform (make-instance 'scene)
    :reader initial-scene)
   (%game-loop
    :type (or null game-loop)
    :initform nil
    :accessor %game-loop)
   (%current-scene
    :type (or null scene)
    :initform nil
    :reader current-scene)
   (%event-manager
    :type ecs:event-manager
    :initform (make-instance 'ecs:event-manager)
    :reader %event-manager)
   (%entity-manager
    :type ecs:entity-manager
    :reader %entity-manager)
   (%system-manager
    :type ecs:system-manager
    :initform (make-instance 'ecs:system-manager)
    :reader %system-manager)
   (e_on-render
    :documentation "Fired whenever a render pass is required. Once per frame, typically. But not necessarily."
    :type event
    :initform (make-instance 'event :name "on-render")
    :reader e_on-render)))

(defdyn *game*
  :documentation "The `game' currently running. Dynamically bound during game callbacks.")

(defmethod initialize-instance :after ((game game) &key)
  (setf (slot-value game '%entity-manager)
        (make-instance 'ecs:entity-manager :event-manager (%event-manager game))))

(defgeneric (setf current-scene) (value object))

(defmethod current-time-seconds ((game game))
  (current-time-seconds (%game-loop game)))

(defmethod current-frame-number ((game game))
  (current-frame-number (%game-loop game)))

(defmethod (setf current-scene) (new-val (game game)
                                 &aux
                                   (old-val (current-scene game)))
  (unless (eq new-val old-val)
    (setf (slot-value game '%current-scene) new-val)
    (when old-val
      (%game-unsetup-scene game old-val))
    (when new-val
      (%game-setup-scene game new-val)))

  (current-scene game))

(defmethod e_time-tick ((game game))
  (e_time-tick (%game-loop game)))

(defmethod e_frame-tick ((game game))
  (e_frame-tick (%game-loop game)))

(defun game-init (game
                  &aux
                    (*game* game)
                    (ecs:*entity-manager* (%entity-manager game)))
  (unless (null (%game-loop game))
    (error "game: already initialized"))

  (setf (%game-loop game) (make-instance 'game-loop))

  (let ((event-manager (%event-manager game))
        (system-manager (%system-manager game))
        (entity-manager (%entity-manager game)))
    (forward-event event-manager :time-tick (e_time-tick game))
    (forward-event event-manager :frame-tick (e_frame-tick game))
    (forward-event event-manager :render (e_on-render game))

    (add-system
     system-manager
     (make-instance
      'mar:input-system
      :entity-manager entity-manager
      :event-manager event-manager
      :input-manager (input:current-input-manager)))

    (add-system
     system-manager
     (make-instance
      'mar:render-system
      :entity-manager entity-manager
      :event-manager event-manager))

    (add-system
     system-manager
     (make-instance
      'mar:physics-system
      :entity-manager entity-manager
      :event-manager event-manager))

    (configure-systems system-manager))

  ;;Go into the initial scene
  (setf (current-scene game) (initial-scene game)))

(defun game-update (game
                    &aux
                      (*game* game)
                      (ecs:*entity-manager* (%entity-manager game)))
  "Perform a single update loop through the game."
  (%game-loop-update (%game-loop game)))

(defun game-render (game renderer
                    &aux
                      (*game* game)
                      (ecs:*entity-manager* (%entity-manager game)))
  "Perform a rendering pass on the game using the given renderer."
  ;;TODO get rid of this HACKY HACK HACK
  (let ((sdl2-renderer (slot-value renderer 'sol.sdl2-driver::%renderer-native)))
    (cffi:with-foreign-objects ((w :int) (h :int))
      (sdl2:sdl-render-get-logical-size sdl2-renderer w h)
      (sdl2:sdl-render-set-logical-size sdl2-renderer 800 608)
      (unwind-protect
           (with-simple-restart (skip-render "Skip this rendering pass.")
             (event-notify (e_on-render game) renderer))
        (sdl2:sdl-render-set-logical-size sdl2-renderer (cffi:mem-ref w :int) (cffi:mem-ref h :int))))))

(defun game-uninit (game
                    &aux
                      (*game* game)
                      (ecs:*entity-manager* (%entity-manager game)))
  (unless (%game-loop game)
    (error "game: already uninitialized"))
  ;;Exit from any scene we might be in
  (setf (current-scene game) nil)

  (unconfigure-systems (%system-manager game))

  (let ((event-manager (%event-manager game)))
    (unforward-event event-manager :render (e_on-render game))
    (unforward-event event-manager :frame-tick (e_frame-tick game))
    (unforward-event event-manager :time-tick (e_time-tick game)))

  (setf (%game-loop game) nil))

(defun %game-setup-scene (game scene)
  (%scene-init scene)
  (values))

(defun %game-unsetup-scene (game scene)
  (%scene-uninit scene)
  (values))

(in-package #:mar)

(defmacro defdyn (name &key (value nil value-p) (documentation nil documentation-p))
  "As `defvar', but allows specifying documentation without binding to a value."
  (if value-p
      `(defvar ,name ,value ,documentation)
      `(progn
         (defvar ,name)
         ,(when documentation-p
            `(setf (documentation ',name 'variable) ',documentation))
         ',name)))

;;Multiplier applied in order to translate from some time amount into milliseconds
(defparameter +internal-time/ms+ (/ internal-time-units-per-second 1000))
(defparameter +internal-time/second+ internal-time-units-per-second)

(defparameter +ms/internal-time+ (/ 1000 internal-time-units-per-second))
(defparameter +seconds/internal-time+ (/ 1 internal-time-units-per-second))

(declaim (inline internal-time->seconds))
(defun internal-time->seconds (time)
  (* time +seconds/internal-time+))

(declaim (inline internal-time->ms))
(defun internal-time->ms (time)
  (* time +ms/internal-time+))

(declaim (inline seconds->internal-time))
(defun seconds->internal-time (seconds)
  (* seconds +internal-time/second+))

(declaim (inline ms->internal-time))
(defun ms->internal-time (ms)
  (* ms +internal-time/ms+))

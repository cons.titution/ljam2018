;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:mar)

(defclass render-system (ecs:system)
  ())

(defmethod initialize-instance :after ((system render-system)
                                       &key
                                       &allow-other-keys))

(defmethod ecs:system-configure ((system render-system))
  (ecs:subscribe-event
   (ecs:event-manager system)
   :time-tick
   system
   '%render-on-time-tick)

  (ecs:subscribe-event
   (ecs:event-manager system)
   :render
   system
   '%render-on-render))

(defmethod ecs:system-unconfigure ((system render-system))
  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :time-tick
   system)

  (ecs:unsubscribe-event
   (ecs:event-manager system)
   :render
   system))

(defun %render-on-time-tick (system dt)
  ;;Update all the animations
  (ecs:do-components (entity-id (sprite components:sprite))
      ()
    (when (components:current-animation sprite)
      (media:update-animation
       (components:current-animation sprite)
       dt))))

(defun %render-on-render (system renderer
                          &aux
                            (x-offset 0)
                            (y-offset 0))
  (ecs:do-components (entity-id
                      (transform components:transform)
                      (camera components:camera))
      ()
    (when (components:activep camera)
      (setf x-offset (max 0 (- (math:vec-x (components:location transform)) (/ (media:renderer-width renderer) 2))))
      (setf y-offset (max 0 (- (math:vec-z (components:location transform)) (/ (media:renderer-height renderer) 2))))))

  (media:render-push-translate
   renderer
   (- x-offset)
   (- y-offset))
  (unwind-protect
       (progn
         (ecs:send-event
          (ecs:event-manager system)
          :on-render
          renderer)

         ;;Draw all the game entities
         (ecs:do-components (entity-id
                             (transform components:transform)
                             (sprite components:sprite))
             ()
           (when (components:current-animation sprite)
             (media:draw-animation
              (components:current-animation sprite)
              renderer
              ;;Draw animation relative to center point of object
              :x (- (math:vec-x (components:location transform))
                    (* (media:animation-width (components:current-animation sprite)) 0.5))
              :y (- (math:vec-z (components:location transform))
                    (* (media:animation-height (components:current-animation sprite)) 0.5))))))
    (media:render-pop renderer)))
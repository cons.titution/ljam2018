(in-package #:mar)

(defclass game-loop ()
  ((%current-time-seconds
    :type real
    :documentation "The current time, in seconds. This updates every time-tick, so should stay the same for a full frame."
    :initform 0.0
    :accessor %current-time-seconds
    :reader current-time-seconds)
   (%current-frame-number
    :type integer
    :documentation "The current frame number."
    :initform 0
    :accessor %current-frame-number
    :reader current-frame-number)
   (e_time-tick
    :documentation "Fired possibly multiple times per frame each clock tick with dt seconds"
    :type event
    :initform (make-instance 'event :name "time-tick")
    :reader e_time-tick)
   (e_frame-tick
    :documentation "Fired once per 'frame', with at least one time tick preceeding it. Called with the current frame number."
    :type event
    :initform (make-instance 'event :name "frame-tick")
    :reader e_frame-tick)
   (%current-real-time
    :documentation "The last time mar was updated, in `real-time' units. See `cl:get-internal-real-time'"
    :type integer
    :initform (get-internal-real-time)
    :accessor %current-real-time)
   (%accumulator-seconds
    :documentation "\"Extra\" time every tick, carried over to the next."
    :type real
    :initform 0.0
    :accessor %accumulator-seconds))
  (:documentation
   "Represents the `mar' game loop."))

(defun %game-loop-time-tick (game-loop dt)
  (incf (%current-time-seconds game-loop) dt)
  (with-simple-restart (skip-time-tick "Skip updating for this time tick.")
    (event-notify (e_time-tick game-loop) dt)))

(defun %game-loop-frame-tick (game-loop)
  (incf (%current-frame-number game-loop))
  (with-simple-restart (skip-frame "Skip updating for frame ~D." (current-frame-number game-loop))
    (event-notify (e_frame-tick game-loop) (current-frame-number game-loop))))

(defun %game-loop-update (game-loop
                          &aux
                            (max-fps 120)
                            (dt-seconds (float (/ 1 max-fps))))
  (let* ((new-time (get-internal-real-time))
         (frame-time-seconds
           (internal-time->seconds
            (- new-time (%current-real-time game-loop)))))
    (setf (%current-real-time game-loop) new-time)

    (incf (%accumulator-seconds game-loop) frame-time-seconds)

    (cond
      ((>= (%accumulator-seconds game-loop) dt-seconds)
       (loop
         :do
            (%game-loop-time-tick game-loop dt-seconds)
            (decf (%accumulator-seconds game-loop) dt-seconds)
         :while
         (and (>= (%accumulator-seconds game-loop) dt-seconds)))
       ;;Update frame
       (%game-loop-frame-tick game-loop))
      (t
       ;;TODO Rather than sleep, we need to yield back control somehow
       ;;Maybe adopt some sort of rx or coroutine approach
       ;;Throttle
       (sleep (- dt-seconds (%accumulator-seconds game-loop)))))))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(in-package #:defpackage+-user-1)

(defpackage+ #:mar
  (:use
   #:alexandria
   #:cl)
  (:inherit
   #:sol
   #:luna
   #:luna.ecs)
  (:local-nicknames
   (#:dispatcher #:sol.dispatcher)
   (#:input #:sol.input)
   (#:media #:sol.media)
   (#:media.colors #:sol.media.colors)
   (#:ui #:sol.ui)
   (#:control #:luna.control)
   (#:ecs #:luna.ecs)
   (#:math #:luna.math)
   (#:physics #:luna.physics)
   (#:serial #:luna.serial)
   (#:shapes #:luna.shapes)
   (#:components #:mar.components)
   (#:sdl2 #:raw-bindings-sdl2))
  (:export
   #:scene
   #:name

   #:game
   #:current-scene
   #:game-init
   #:game-update
   #:game-render
   #:game-uninit
   #:e_time-tick
   #:e_frame-tick
   #:e_on-render

   #:game-control
   #:game

   #:current-time-seconds
   #:current-frame-number

   #:input-system

   #:physics-system
   #:collision-event
   #:collision-event-td
   #:collision-event-mtd-dir
   #:collision-event-collider

   #:render-system))

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(eval-when (:load-toplevel :compile-toplevel :execute)
  (ql:quickload :cl-fad :silent t)
  (ql:quickload :cl-ppcre :silent t)
  (ql:quickload :cl-interpol :silent t)
  (ql:quickload :command-line-arguments :silent t)
  (ql:quickload :split-sequence :silent t)

  (asdf:load-asd (merge-pathnames "build.asd"))
  (ql:quickload :build :silent t))

(cl-interpol:enable-interpol-syntax)

(defparameter *me* (pathname-name *load-truename*))
(defparameter *me-dir* (uiop:pathname-directory-pathname *load-truename*))
(defparameter *root-dir* (uiop:pathname-parent-directory-pathname *me-dir*))

(defparameter *out-dir* (build:resolve-relative #P"out/" *root-dir*))
(defparameter *fflibs-dir*  (build:resolve-relative #P"fflibs/" *root-dir*))
(defparameter *data-dir*  (build:resolve-relative #P"data/" *root-dir*))
(defparameter *debug-build* nil)
(defparameter *version-number* "0.0.0")

(defun build-escaped-path (part &rest more-parts)
  (format
   nil
   "~{~A~^\\\\~}"
   (mapcan
    (lambda (part)
      (split-sequence:split-sequence
       #\\
       (substitute #\\ #\/ (namestring part))
       :remove-empty-subseqs t))
    (cons part more-parts))))

(defun death-pong ()
  (format t "make: building death-pong~%~%")

  (let ((code
         (build:build
          :loaded-systems '(:ljam2018)
          :toplevel-package-name "LJAM2018"
          :output-path (build:resolve-relative "death-pong/death-pong.exe" *out-dir*)
          :executable t
          :gui (not *debug-build*)
          :debug-build *debug-build*)))
    (cond
      ((zerop code)
       (format t "~%make: build successful~%~%")
       0)
      (t
       (format *error-output* #?"make: error in build: ${code}")
       code))))

(defun data-copy ()
  (format t "make: copying data~%")

  (let* ((out
          (build-escaped-path
           *out-dir*
           "death-pong/data"))
         (code
          (process-exit-code
           (run-program
            "robocopy"
            (list (uiop:native-namestring *data-dir*) out "/E" "/XO")
            :search t
            :input t
            :output t
            :error t))))
    ;;See https://ss64.com/nt/robocopy-exit.html for exit codes
    ;;notably, 1 is "everything went okay"
    (cond
      ((or (= code 0)
           (= code 1))
       (format t "make: copying successful~%")
       0)
      (t
       (format *error-output* #?"make: error in copy: ${code}~%")
       2))))

(defun dll-copy ()
  (let ((output-dir (truename (build:resolve-relative "death-pong/" *out-dir*))))
    (format t #?"make: copying dependent libraries from ${*fflibs-dir*} to ${output-dir}~%~%")

    (dolist (d (mapcar
                (lambda (d)
                  (build:resolve-relative "bin/Windows/x64/" d))
                (cl-fad:list-directory *fflibs-dir*)))
      (dolist (bin-file (cl-fad:list-directory d))
        (format t #?"make: copying ${bin-file}~%")
        (cl-fad:copy-file
         bin-file
         (merge-pathnames
          (make-pathname
           :name (pathname-name bin-file)
           :type (pathname-type bin-file))
          output-dir)
         :overwrite t)))
    (format t  "~%make: finished copying files~%")
    0))

(defun main (args)
  (let ((targets '(death-pong
                   dll-copy
                   data-copy)))
    (let ((unrecognized-args ()))
      (loop
         :for arg in (cdr args)
         ;;Debug build
         :when (cl-ppcre:scan "^-d" arg)
         :do (setf *debug-build* t)
         ;;Output directory
         :else :when (cl-ppcre:scan "^-o" arg)
         :do (setf *out-dir* (cl-fad:pathname-as-directory (subseq arg 2)))
         :else :when (cl-ppcre:scan "^-v" arg)
         :do (let* ((ver-string (subseq arg 2)))
               (unless (cl-ppcre:scan "^(?:(\\d+)\\.)?(?:(\\d+)\\.)?(\\d+)$" ver-string)
                 (format *error-output*
                         "make: invalid version number '~A'" ver-string)
                 (return-from main 1))
               (destructuring-bind (major &optional (minor 0) (patch 0))
                   (mapcar #'parse-integer (split-sequence:split-sequence #\. ver-string))
                 (unless (<= 0 major 255)
                   (format *error-output*
                           "make: major version must be between 0 and 255~%")
                   (return-from main 1))
                 (unless (or (null minor)
                             (<= 0 minor 255))
                   (format *error-output*
                           "make: minor version must be between 0 and 255~%")
                   (return-from main 1))
                 (unless (or (null patch)
                             (<= 0 patch 65535))
                   (format *error-output*
                           "make: patch version must be between 0 and 65535~%")
                   (return-from main 1))

                 (setf *version-number* (format nil "~A.~A.~A" major minor patch))))
         :else :do (push arg unrecognized-args))

      (when unrecognized-args
        (setf unrecognized-args (nreverse unrecognized-args))
        (format *error-output*
                "make: unrecognized arg~P:~{~&  ~A~}"
                (length unrecognized-args)
                unrecognized-args)
        (return-from main 1)))
    ;;Normalize out-dir
    (setf *out-dir* (build:resolve-relative *out-dir*))
    (loop
       :for target :in targets
       :for code :=
       (handler-case
           (funcall target)
         (error (c)
           (format *error-output* "~%make: error while executing target '~A':~%~A~%" target c)
           -1))
       :when (not (zerop code))
       :return code
       :finally
       (return 0))))

(exit :code (main *posix-argv*))

(cl-interpol:disable-interpol-syntax)

;;;Copyright (c) 2017 Wilfredo Velázquez-Rodríguez
;;;
;;;This software is provided 'as-is', without any express or implied
;;;warranty. In no event will the authors be held liable for any damages
;;;arising from the use of this software.
;;;
;;;Permission is granted to anyone to use this software for any purpose,
;;;including commercial applications, and to alter it and redistribute
;;;it freely, subject to the following restrictions:
;;;
;;;1. The origin of this software must not be misrepresented; you must not
;;;   claim that you wrote the original software. If you use this software
;;;   in a product, an acknowledgment in the product documentation would
;;;   be appreciated but is not required.
;;;
;;;2. Altered source versions must be plainly marked as such, and must not
;;;   be misrepresented as being the original software.
;;;
;;;3. This notice may not be removed or altered from any source distribution.

(defpackage #:build
  (:use #:alexandria #:cl)
  (:export
   #:with-current-directory
   #:resolve-relative
   #:build))

(in-package #:build)

(defmacro with-current-directory (dir &body body)
  (with-gensyms (orig-dir-sym)
    `(let* ((,orig-dir-sym (sb-posix:getcwd))
            (*default-pathname-defaults* ,dir))
       (sb-posix:chdir (namestring *default-pathname-defaults*))
       (unwind-protect
            (progn
              ,@body)
         (sb-posix:chdir ,orig-dir-sym)))))

(defun resolve-relative (path &optional (default *default-pathname-defaults*))
  (namestring
   (merge-pathnames
    path
    (make-pathname
     :host
     (pathname-host default)
     :device
     (pathname-device default)
     :directory
     (pathname-directory default)))))

(defun build (&key
                (image (first sb-ext:*posix-argv*))
                (core sb-ext:*core-pathname*)
                (loaded-systems ())
                (loaded-files ())
                (executable nil)
                (gui nil)
                (toplevel-symbol-name "MAIN-EXE")
                (toplevel-package-name "COMMON-LISP-USER")
                (output-path (if executable "a.exe" "a.core"))
                (debug-build nil))
  ;;Parse any command-line args
  (setf output-path
        (resolve-relative output-path))

  ;;Diagnostics
  (format t "build: image: ~A~%" image)
  (when loaded-systems
    (format t "build: using systems:~{~&  ~A~}~%" loaded-systems))
  (when loaded-files
    (format t "build: additionally loading:~{~& ~A~}~%" loaded-files))
  (cond
    (executable
     (format t "build: outputting executable as ~A~%" output-path)
     (format t "build: application is ~:[CONSOLE~;GUI~]~%" gui)
     (format t "build: toplevel is ~A::~A~%" toplevel-symbol-name toplevel-package-name)
     (when debug-build
       (format t "build: debug build~%")))
    (t
     (format t "build: outputting core as ~A~%" output-path)))

  (cl-fad:with-open-temporary-file (f :direction :output :if-exists :supersede)
    (format t "build: creating bootstrap at ~A~%" (namestring f))
    (format t "~2&==BEGIN BOOTSTRAP CODE==~2%")
    (let ((*standard-output* (make-broadcast-stream *standard-output* f)))
      ;;Generate code to use the standard packages
      (print '(in-package :cl-user))

      ;;Generate code to load systems
      (when loaded-systems
        (print
         `(ql:quickload ',loaded-systems)))

      ;;Generate code to load additional files
      (loop :for file :in loaded-files
         :do (print `(eval-when (:compile-toplevel :load-toplevel :execute) (load ,file))))

      ;;Make sure the directory we want to output the core to exists
      (print `(ensure-directories-exist ,output-path))
      ;;Clear the ASDF source registry to force it to re-initialize properly
      (print `(asdf:clear-source-registry))

      ;;Make sure the toplevel symbol exists
      (cond
        (executable
         ;;Verify toplevel symbol exists
         (print
          (let ((toplevel-sym (intern "SYM" "CL-USER")))
            `(let ((,toplevel-sym
                    (find-symbol ,toplevel-symbol-name ,toplevel-package-name)))
               (unless ,toplevel-sym
                 (format *error-output*
                         "~%bootstrap: cannot find symbol '~A' in package '~A'~%"
                         ,toplevel-symbol-name ,toplevel-package-name)
                 (sb-ext:exit :code 1))
               (unless (fboundp ,toplevel-sym)
                 (format *error-output*
                         "~%bootstrap: symbol '~A' is not fboundp" ,toplevel-sym)
                 (sb-ext:exit :code 2))

               ;;Re-enable the debugger..
               ,@(when debug-build
                   `((sb-ext:enable-debugger))
                   ())

               ;;Dump exe
               (sb-ext:save-lisp-and-die
                ,output-path
                :executable t
                :application-type ,(if gui :gui :console)
                :toplevel ,toplevel-sym)))))
        (t
         ;;Re-enable the debugger..
         (print `(sb-ext:enable-debugger))

         ;;Dump core
         (print `(sb-ext:save-lisp-and-die ,output-path))))

      ;;Finish output to the file
      (terpri)
      (finish-output))
    (format t "~3&==END BOOTSTRAP CODE==~2%")

    (format t "build: running sbcl as ~A~%" image)
    (when core
      (format t "build: using core ~A~%" core))

    (let ((sbcl-args (list
                      (when core "--core")
                      (when core (namestring core))
                      "--noinform"
                      "--end-runtime-options"
                      "--disable-debugger"
                      "--load" (namestring f)
                      "--eval" "(exit :code 1)")))
      (setf sbcl-args (delete nil sbcl-args))
      (format t "build: sbcl args: ~{~&  ~A~}~%" sbcl-args)

      (let (code)
        (format t "~2&==BEGIN BOOTSTRAP==~2%")

        (setf code
              (sb-ext:process-exit-code
               (sb-ext:run-program
                image sbcl-args
                :search t
                :input nil
                :output t
                :error :output)))

        (format t "~2&==END BOOTSTRAP==~2%")

        (unless (zerop code)
          (format t "build: bootstrapping failed, exited with code ~A~%" code)
          (finish-output)
          (return-from build 2))))

    (format t "build: compilation finished~%"))
  (finish-output)
  0)

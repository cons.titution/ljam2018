# Death Pong

![Death Pong Banner](data/images/banner/pong.png)

## Overview

​​Entry for the [2018 Lisp Game Jam​](https://itch.io/jam/lisp-game-jam-2018)

Step into a ​world​ where incredible circumstance has led humankind to become little more than entertainment for advances artificial intelligence.

Play the role of a Tribute. A person slated for repeated death at the hands of incredibly archaic technologies not even worthy of the title of 'artificial intelligence'. Stay out of their way as they do what they were programmed to do: Play Pong at a mediocre level.

Dodge the inconsequential ball headed your way, and watch your points wrack up as you disturb the play field in ways these paddles were not prepared for.

Just remember: There is no victory.  Only points. And death. If I can get that programmed in before this is due.

**DEATH PONG**

## Developers

* [Zulu-Inuoe](https://github.com/Zulu-Inuoe)
* ​[spreadLink](https://github.com/spreadLink)

## Supported Platforms

Developed on [SBCL](http://www.sbcl.org/).

Should run on other implementations with ffi support, though no guarantees.

## Runtime Dependencies

* [libffi](https://sourceware.org/libffi/)
* [SDL2](https://www.libsdl.org/)
* [SDL2_gfx](http://www.ferzkopp.net/Software/SDL2_gfx/Docs/html/index.html)
* [SDL2_image](https://www.libsdl.org/projects/SDL_image/)
* [SDL2_ttf](https://www.libsdl.org/projects/SDL_ttf/)

## License

See [LICENSE.txt](LICENSE.txt)
